----------------------------------------------------------------------------------
-- Company: A1K
-- Engineer: Matthias Heinrichs
-- 
-- Create Date:    09:00  2020/01/13
-- Design Name:    68060-68030-Statemachine with DMA
-- Module Name:    control - Behavioral 
-- Project Name:   A4000-TK
-- Target Devices: XC95288XL-TQFP144-10ns
-- Revision: 0.1
-- Additional Comments: 
--
-- SET TABS TO FOUR!
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control is
	 Port (   --clocks
			  CLK30 : in  STD_LOGIC;								--CLK from 68030-Bus
			  BUS_CLK : in STD_LOGIC;								--Bus CLK from 040-CPU
			  
			  --Reset
			  RSTO40 : in  STD_LOGIC;								--Reset-out from 040-CPU 
			  RSTI40 : out  STD_LOGIC;								--Reset-in from 040-CPU
			  RESET30 : inout  STD_LOGIC;							--Reset from/to 68030-Bus
			  
			  --68030-BUS
			  AL	: inout STD_LOGIC_VECTOR (1 downto 0);			--Address A0-1 to 68030-Bus
			  AS30 : inout  STD_LOGIC;								--Address-Strobe to 68030-Bus
			  DS30 : inout  STD_LOGIC;								--Data-Strobe to 68030-Bus
			  RW30 : inout  STD_LOGIC;								--Read/Write to 68030-Bus
			  FC30 : out  STD_LOGIC_VECTOR (2 downto 0);			--Function-code 0-2 to 68030-Bus
			  SIZ30 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer size bits 0-1 to 68030-Bus
			  IPL30 : in  STD_LOGIC_VECTOR (2 downto 0);			--Interrupt Prio-Level 0-2 from 68030-Bus
			  DSACK30 : in  STD_LOGIC_VECTOR (1 downto 0);			--DSACK Bit 0/1 from 68030-Bus
			  DSACK30out : out  STD_LOGIC;							--DMA-Cycle end out to 68030-Bus
			  STERM30 : in  STD_LOGIC;								--STERM from 68030-Bus
			  BERR30 : in  STD_LOGIC;								--Bus-Error from 68030-Bus
			  AVEC30 : in  STD_LOGIC;								--Cache-Inhibit from 68030-Bus
			  DMA_CYCLE :out STD_LOGIC;								--DMA-cycle flag to RAM-controller
			  
			  --68040-BUS
			  A40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Address 0-1 from 040-CPU
			  TS40 : inout  STD_LOGIC;								--Transfer-Start from 040-CPU
			  RW40 : inout  STD_LOGIC;								--Read/Write from 040-CPU
			  TM40 : inout  STD_LOGIC_VECTOR (2 downto 0);			--Transfer Modifier Bit 0-3 from 040-CPU
			  TT40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer-Type Bit 0-1 from 040-CPU
			  SIZ40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer-Breite Bit 0-1 from 040-CPU
			  IPL40 : out  STD_LOGIC_VECTOR (2 downto 0);			--Interrupt Prio-Level 0-2 to 040-CPU
			  TBI40 : out  STD_LOGIC;								--Transfer Burst Inhibit to 040-CPU
			  TCI40 : out  STD_LOGIC;								--Transfer Cache Inhibit to 040-CPU
			  TA40 : inout  STD_LOGIC;								--Transfer Acknowledge to 040-CPU
			  TEA40 : out  STD_LOGIC;								--Transfer Error Acknowledge to 040-CPU
			  AVEC40 : out  STD_LOGIC;								--Autovectorcycle to 040-CPU
			  
			  --RAM
			  RAM_ACCESS : in  STD_LOGIC;							--RAM_ACCESS indicator
			  TA_RAM : in  STD_LOGIC;								--Transfer Acknowledge from RAM controller		    
			  CACHE_INHIBIT: in std_logic;							--Cache controll
			  
			  --Bus Sizing
			  OE_BS : out  STD_LOGIC;								--Output-Enable for 68030-68040 bus-sizing
			  LE_BS : out  STD_LOGIC;								--Latch-Enable for 68030-68040 bus-sizing
			  BWL_BS : out  STD_LOGIC_VECTOR (2 downto 0);			--BYTE/WORD/LONG Bus-Sizing Bit 0-3

			  --DMA
			  BR30 :  in  STD_LOGIC;								--Busrequest 68030-Bus
			  BGACK30 :  in  STD_LOGIC;								--Bus Grant ack 68030-Bus		  
			  BG30 : out  STD_LOGIC;								--Busgrant 68030-Bus
			  BR40 :  in  STD_LOGIC;								--Busrequest 68040-Bus
			  BG40 :  out  STD_LOGIC;								--Busgrant 68040-Bus
			  BB40 :  inout  STD_LOGIC;								--Busbusy 68040-Bus
			  SNOOP60 : out  STD_LOGIC);							--Bus-Snoop-operation for DMA invalidation (68060 only)
				
end control;

architecture Behavioral of control is

	TYPE sm_68030_P IS (
				S0,
				S2,
				S4
				);
								
	TYPE sm_68030_N IS (
				S1,
				S3,
				S5
				);

	TYPE sm_68030 IS (
				S0_P,
				S1_N,
				S2_P,
				S3_N,
				S4_P,
				S5_N
				);
				
	--byteorder: D31-D0: byte3,byte2,byte1,byte0
	--wordorder: D31-D0: high_word,low_word
	TYPE sm_sizing IS (
				--idle,				--000
				size_decode,	--001
				get_byte2,		--010
				get_byte1,		--100
				get_byte0,		--100
				get_low_word	--011				
				);
	TYPE sm_dma IS (
				ARBIT_BUS,				
				ASSERT_BG_030,				
				WAIT_FOR_BGACK,				
				RELEASE_BG030,				
				BUS_MASTER_030,			
				SECOND_030_DMA_REQUEST,				
				WAIT_FOR_BGACK_FINISH,
				PRE_BUS_MASTER_040,
				BUS_MASTER_040,
				RELEASE_BG040,
				WAIT_BUS_FREE_040_2,
				WAIT_BUS_FREE_040				
				);
				
constant CLOCK_SAMPLE : integer := 0; 

signal	DMA_SM: sm_dma := ARBIT_BUS;
signal	SM030_N : sm_68030_N;	--State-Machine negative CPU-Flanke
signal	SM030_P : sm_68030_P;	--State-Machine positive CPU-Flanke
signal	SIZING : sm_sizing := size_decode;	--State-Machine Sizing
signal	SIZ30_D : STD_LOGIC_VECTOR (1 downto 0):="11";	--SIZ30-Signal
signal	AL_D : STD_LOGIC_VECTOR (1 downto 0):="11";		--Lower Address-Signal
signal	RSTINT : STD_LOGIC:='0';	--Reset delayed 1 BCLK
signal	RSTINT_D1 : STD_LOGIC:='0';	--Reset delayed 2 BCLK
signal	AMISEL : STD_LOGIC:='0';	--Selects the 68030-side
signal	TA40_SIG : STD_LOGIC:='0';	--Transfer acknowledge to 68040
signal	LE_DMA_SIG : STD_LOGIC:='0';	--latch enable during DMA
signal	AS30_SIG : STD_LOGIC:='0';	--68030-address strobe signal
signal	DS30_SIG : STD_LOGIC:='0';	--68030-data strobe signal
signal	RSTI40_SIG : STD_LOGIC:='0';	--internal reset detection / controll
signal	BYTE : STD_LOGIC:='0'; --Helper signal BYTE-access
signal	WORD : STD_LOGIC:='0'; --Helper signal WORD-access
signal	LONG : STD_LOGIC:='0'; --Helper signal LONG-access
signal	BGACK30_Q : STD_LOGIC:='1';  --BGACK30 synced on 68030-bus
signal	BR30_Q : STD_LOGIC:='1';  	--BR30 synced on 68030-bus
signal	BG30_SIG : STD_LOGIC:='1';  --busgrant 68030 signal
signal	BG40_SIG : STD_LOGIC:='1'; --busgrant 60404 signal
signal	BG40_SIG_Q : STD_LOGIC:='1'; --bg40_Q synced on 868040-clock
signal	BR40_Q : STD_LOGIC_VECTOR (1 downto 0):="11"; --BR40 synced on 68030-bus
signal	BB40_Q : STD_LOGIC_VECTOR (1 downto 0):="11"; --BusBusy40 synced on 68030-bus
signal	LE_BS_SIG_N : STD_LOGIC:='0';  	--LE_BS for read
signal	LE_BS_SIG_P : STD_LOGIC:='0';  	--LE_BS for write
signal 	START_SEND : STD_LOGIC:='0'; --68030-start toggle bit in 68040-clock domain
signal 	START_SEND_SAMPLED : STD_LOGIC:='0'; --68030-start in 68030-dclock omain
signal 	START_ACK : STD_LOGIC:='0'; --68030-start acknowledge 
signal 	END_SEND : STD_LOGIC:='0';
signal 	END_ACK : STD_LOGIC:='0';
signal 	RESETI_S : STD_LOGIC:='1';	--sampled RESET IN
signal 	DATA_OE_DMA : STD_LOGIC:='0';
signal 	DS_D0_DMA : STD_LOGIC_VECTOR (1 downto 0):="11";
signal 	STERM_DMA : STD_LOGIC:='1';
signal 	STERM_DMA_30 : STD_LOGIC:='1';
signal	FC30_D : STD_LOGIC_VECTOR (2 downto 0):="111";			--Function code buffer
signal	BWL_BS_D : STD_LOGIC_VECTOR (2 downto 0):="111";		--Bussize buffer
signal 	TS40_DMA : STD_LOGIC:='1'; -- 68040 Transferstart during DMA
signal	TEA_SIG : STD_LOGIC:='1';  --68040-tranmsfewr error ack
signal	RW30_Q : STD_LOGIC:='1'; --RW- sample to keep it stable during one cycle
signal 	AS_D : STD_LOGIC_VECTOR (4 downto 0):="11111";
signal	DSACK_TERM : STD_LOGIC_VECTOR (1 downto 0):="11";	--buffered DSACK for termination size calculation
signal	AS30_D : STD_LOGIC:='1'; --Delayed AS for edge detection BUS_CLK
signal	AS30_D_C : STD_LOGIC:='1'; --Delayed AS for edge detection Amiga clock


-- Acceptable values for this in ISE are: one-hot compact speed1 sequential johnson gray and auto.
-- Acceptable values for this in vivado are: one_hot sequential johnson gray and auto.
-- NOTE THE DIFFERENT SPELLING OF "one hot"
attribute fsm_encoding : string;
attribute fsm_encoding of SM030_N : signal is "gray"; --other than gray do not work
attribute fsm_encoding of SM030_P : signal is "gray"; --other than gray do not work
attribute fsm_encoding of DMA_SM : signal is "compact";
attribute fsm_encoding of SIZING : signal is "compact";


	Function to_std_logic(X: in Boolean) return Std_Logic is
	variable ret : std_logic;
	begin
	if x then ret := '1';  else ret := '0'; end if;
	return ret;
	end to_std_logic;


	-- sizeIt replicates a value to an array of specific length.
	Function sizeIt(a: std_Logic; len: integer) return std_logic_vector is
		variable rep: std_logic_vector( len-1 downto 0);
	begin for i in rep'range loop rep(i) := a;  end loop; return rep;
	end sizeIt;
begin

	
	--Reset generation
	
	--Send reset
	RESET_40O: process (BUS_CLK)
	begin
		if(rising_edge(BUS_CLK)) then
			if(RSTO40 ='0' )then
				RESETI_S <= '0';
			else
				RESETI_S<=RESET30;
			end if;
		end if;
	end process;
	
	RESET30	<=	'0' when RSTO40 ='0' else 'Z'; 

	--Receive reset
	RESET_40I: process (BUS_CLK)
	begin
		if(rising_edge(BUS_CLK)) then
			RSTINT_D1	<=RESET30;
		
			if(RSTO40 = '1' and RESETI_S = '0' ) then
				RSTI40_SIG	<=	'0';
				RSTINT	<='0';
				
			else 
				if(RESETI_S ='1')then
					RSTI40_SIG	<=	'1';
					RSTINT		<=RSTI40_SIG;
					RSTINT_D1	<=RSTINT;
				end if;
			end if;
		end if;
	end process RESET_40I;

	RSTI40 <= RSTI40_SIG;

	-- CPU configuration and interrupt sampling
	INT_SAMPLE: process(RSTINT_D1,CLK30)
	begin
		if(RSTINT_D1='0')then
			IPL40 <= "111"; ---must be "000" for 68040 and "111" for 68060!!!!
		elsif(rising_edge(CLK30))then
			IPL40 <= IPL30;
		end if;
	end process;

-- Signaldesctiption
--	1  0	/DSACK						SIZ1 SIZ0  Size030	FC2 FC1 FC0  Space
--	---------------------------	------------------	------------------
--	H  L	 8 Bit-Port (D31-D24)	0    1	   Byte		1   1   1    CPU
--	L  H	16 Bit-Port (D31-D16)	1    0	   Word		0   0   1    USR Data
--	L  L	32 Bit-Port (D31-D00)	1    1	   3 Bytes	0   1   0    USR Prog
--									0    0	   Long		1   0   1    SUP Data
--														1   1   0    SUP Prog

--	SIZ1 SIZ0  Size040	TT1 TT0   Type	  TM2 TM1 TM0
--	------------------	--------------	  --------------
--	0    1     Byte		0   0   normal	  0   0   0   Data Cache Push
--	1    0     Word		0   1   MOVE16	  0/1 0   1   Usr/SupV data
--	1    1     Line		1   0   AltAcc	  0/1 1   0   Usr/supV code
--	0    0     Long		1   1   Acknow	  0,1 1,0 1,0 MMU data,code
--										  	1   1   1   reserved

	--Hilfssignale
	DMA_CYCLE <= AS30 when BGACK30 ='0' else '1';
	BYTE 	<= '1' when SIZ40 = "01" else '0';
	WORD 	<= '1' when SIZ40 = "10" else '0';
	LONG 	<= '1' when SIZ40 = "11" or SIZ40 ="00" else '0';
	AMISEL	<= '1' when (TT40(1) = '0' and RAM_ACCESS ='1') 	-- address 68030
											 or TT40(1)='1' 	-- alt func AVEC/BRKPT
						 else '0';
	
	-- Signal assignment 68030 side
	RW30	<= RW30_Q 	when BGACK30 ='1' else 'Z';
	SIZ30	<= SIZ30_D 	when BGACK30 ='1' else "ZZ";
	AL 		<= AL_D 	when BGACK30 ='1' else "ZZ";
	DSACK30out <= STERM_DMA_30;
	AS30	<= AS30_SIG when BGACK30 ='1' else 'Z';
	DS30	<= DS30_SIG when BGACK30 ='1' else 'Z';
	FC30	<= FC30_D 	when BGACK30 ='1' else "ZZZ";
	BG30 	<= BG30_SIG;

	-- Signal assignment 68040 side

	TCI40	<= '1' when 	CACHE_INHIBIT ='0' or 
							( TT40(1) = '0' and (TM40 ="010" or TM40 = "110")) 
					else '0'; -- !TT1 -> normal/move16 TM2..0 -> user code access   // !TT1 -> normal/move16 TM2..0 -> supervisor code access

	RW40	<= RW30_Q 	when BGACK30 ='0' else 'Z';
	SIZ40	<= SIZ30_D 	when BGACK30 ='0' else "ZZ";
	A40 	<= AL_D   	when BGACK30 ='0' else "ZZ";
	BB40 	<= '0' 		when BGACK30 ='0' else 'Z';		

	SNOOP60 <= '0' 		when BGACK30 ='0' else '1';
	TS40 	<= TS40_DMA	when BGACK30 ='0' else 'Z';
	TT40 	<= "00" 	when BGACK30 ='0' else "ZZ";
	TM40 	<= "001" 	when BGACK30 ='0' else "ZZZ";
	
	TA40 	<= TA40_SIG when AMISEL = '1' and BGACK30 ='1' else
			   TA_RAM;--   when AMISEL = '0' else 'Z';

	TBI40	<= not AMISEL; --burst only local ram!
	
	TEA40	<= TEA_SIG;
	AVEC40 	<= 'Z'; --not supported yet
	BG40 	<= BG40_SIG_Q;


	-- Signal assignment bussizing
	OE_BS	<= 	'0' when RESET30='0' else
				RAM_ACCESS when BGACK30 = '1' else DATA_OE_DMA;
	LE_BS <= to_std_logic(
					(LE_BS_SIG_P = '1') or --normal busmaster-operation : write
					(LE_BS_SIG_N = '1') or --normal busmaster-operation : read
					(LE_DMA_SIG = '1') -- DMA operation
					);
	--bussizing assign
	BWL_BS <= BWL_BS_D;

	LE_DMA_SAMPLE: process(RESET30, BUS_CLK)
	begin
		if(RESET30='0')then
			LE_DMA_SIG <='0';
			DATA_OE_DMA <='0';
			AS30_D <='1';
		elsif(rising_edge(BUS_CLK))then
			AS30_D <= AS30;
			
			if(AS30_D= '1' and AS30 ='0' and BGACK30 ='0')then --sample during start of cycle to keep this signal stable for the whole cycle
				DATA_OE_DMA <= not RAM_ACCESS;
			elsif(--AS30_D ='1' and 
				AS30='1') then --deassert one BUS_CLK delayed
				DATA_OE_DMA <= '0';
			end if;
			
			if(	BGACK30 = '0' and (
				(DS30 = '0' and TA_RAM = '0' and RW30 = '1') or -- this is doing the magic bus latch trick on DMA read
				(DS30 = '0' and RW30 = '0') -- this is doing the magic bus latch trick on DMA write
				))then
				LE_DMA_SIG <= not RAM_ACCESS;
			else
				LE_DMA_SIG <= '0';
			end if;
		end if;
	end process LE_DMA_SAMPLE;
	
	--030 cycle synchronization: 040 side
	START_STOP_SAMPLE: process(RESETI_S,BUS_CLK)
	begin
		if(RESETI_S ='0')then
			START_SEND <='0';
			END_ACK <= '0';
			TA40_SIG <='1';
			TEA_SIG  <='1';
		elsif(rising_edge(BUS_CLK))then
			--toggle signal for new transfer
			if(TS40 ='0' and TT40(1)='0' and RAM_ACCESS ='1' and BGACK30='1') then
				START_SEND <= not START_SEND;
			end if;
			--detect toggle for end transfer
			if(END_ACK /= END_SEND --normal 68030-cycle termination
				or (TS40 ='0' and TT40(1)='1') --Avec termination /Acknow
				)
			then
				TA40_SIG <= not BERR30; --An 68040 wants '1' if BERR is asserted!
				TEA_SIG  <= BERR30;				
				END_ACK <= END_SEND;
			else
				TA40_SIG <='1';
				TEA_SIG  <='1';
			end if;
		end if;
	end process START_STOP_SAMPLE;

	--DMA 040-Stuff/Arbitter

	DMA40_SAMPLE: process(RESETI_S,BUS_CLK)
	begin
		if(RESETI_S ='0')then
			BR40_Q <= "11";
			BB40_Q <= "11";

		elsif(rising_edge(BUS_CLK))then
			BR40_Q <= BR40_Q(0) & BR40;
			if(BB40 ='0') then
				BB40_Q <= "00";
			else
				BB40_Q <= BB40_Q(0) & BB40;
			end if;
		end if;
	end process DMA40_SAMPLE;
	
	
--			BR40_Q <= BR40;
--			BB40_Q <= BB40;
	
	DMA40_GRANT: process(RESETI_S,BUS_CLK)
	begin
		if(RESETI_S ='0')then
			BG40_SIG_Q <= '1';
		elsif(falling_edge(BUS_CLK))then
			BG40_SIG_Q <= BG40_SIG;
		end if;
	end process DMA40_GRANT;
	TS40_DMA <= '0' when DS_D0_DMA= "10" else '1';

	--DMA into TK-RAM: Transfer start generation
	DS_DMA_SAMPLE: process(RESETI_S,BUS_CLK)
	begin
		if(RESETI_S = '0' )then
			DS_D0_DMA	<= "11";
		elsif(falling_edge(BUS_CLK))then	
			if(BGACK30='1')then
				DS_D0_DMA	<= "11";
			else
				DS_D0_DMA(0)	<= DS30;
				DS_D0_DMA(1)	<= DS_D0_DMA(0);
			end if;
		end if;
	end process DS_DMA_SAMPLE;

	--latch TS_RAM
	DSACK_DMA_SAMPLE: process(RESETI_S,BUS_CLK)
	begin
		if(RESETI_S ='0')then
			STERM_DMA	<= '1';
		elsif(rising_edge(BUS_CLK))then	
			if(BGACK30='1' or AS30='1')then
				STERM_DMA	<= '1';
			elsif(TA_RAM = '0')then	
				STERM_DMA <= '0';
			end if;
		end if;
	end process DSACK_DMA_SAMPLE;
	
	--from now on we have processes working with the Amiga clock

	--DMA 030/040-Arbitter
	DMA_ARBIT: process (CLK30,RESETI_S)
	begin
		if(RESET30 = '0')then
			DMA_SM <=ARBIT_BUS;		
			BG30_SIG <= '1';
			BG40_SIG <= '1';
			BGACK30_Q <= '1';
			BR30_Q <= '1';
		elsif(rising_edge(CLK30)) then
			--sample signals to 68030-clock
			BGACK30_Q <= BGACK30;
			BR30_Q <= BR30;



			--default values
			BG30_SIG <= '1';
			BG40_SIG <= '1';
			
			--MC 68040 Designers Handbook (MC DH) Figure 7-12
			case DMA_SM is
			when ARBIT_BUS =>  --MC DH State 0
				-- This is the idle state.  If neither master wants the
				-- bus, we stick around here.  As soon as one does, we
				-- jump to either's particular mastership branch. 
				if(BGACK30_Q = '0') then --030-BUS (again) in process
					DMA_SM <= BUS_MASTER_030; 
				elsif(BR30_Q = '0') then --030-Bus request
					DMA_SM <= ASSERT_BG_030;
				elsif(BR40 = '0')then --040-Bus request
					DMA_SM <= PRE_BUS_MASTER_040;
				else
					DMA_SM <= ARBIT_BUS;
				end if;
			when ASSERT_BG_030 =>   --MC DH State 1
				--bg must be low for at least two clocks!
				BG30_SIG <= '0';
				DMA_SM <= WAIT_FOR_BGACK;
			when WAIT_FOR_BGACK =>  --MC DH State 2
				-- At this stage, we wait for a bus grant acknowledge back
				-- from the '030 bus.  Upon receipt of that, or negation of
				-- the '030 request, we go on to the next state. 
				BG30_SIG <= '0';
				if(BGACK30_Q = '0') then --030-BUS Ack in process
					DMA_SM <= RELEASE_BG030; 
				elsif(BR30_Q = '1') then --030-Bus request released: end of 030 cycle
					DMA_SM <= RELEASE_BG030;
				else
					DMA_SM <= WAIT_FOR_BGACK; -- wait
				end if;
			when RELEASE_BG030 => --this state simply drops BG to fullfill the bgack to bg timing
				DMA_SM <= BUS_MASTER_030;
			when BUS_MASTER_030 => --MC DH State 4
				-- This is the main '030-as-master running state.  As long as
				-- the '030 bus is master and no new grants some in, we hang 
				-- out here.  If BGACK goes away, the arbiter goes back to 
				-- to the idle state.  If a new bus request is asserted, a
				-- grant must be presented to that master.
				if(BR30_Q = '0' and BR30 = '0') then -- Another 030-BUS request 
					DMA_SM <= SECOND_030_DMA_REQUEST; 
				elsif(BGACK30_Q = '1' and BGACK30 = '1') then --end of 030 cycle
					DMA_SM <= ARBIT_BUS;
				else
					DMA_SM <= BUS_MASTER_030; --030-Bus cycle pending:wait
				end if;
			when SECOND_030_DMA_REQUEST => --MC DH State 5
				--bg must be low for at least two clocks!
				BG30_SIG <= '0';
				DMA_SM <= WAIT_FOR_BGACK_FINISH;	
			when WAIT_FOR_BGACK_FINISH => --MC DH State 6
				-- This state holds bus grant to the '030 bus active, waiting
				-- for either the current '030 master to negate BGACK, or the
				-- new '030 master to negate bus request. 
				BG30_SIG <= '0';
				if(BR30_Q = '1') then -- No BR anymore: drop BG 
					DMA_SM <= RELEASE_BG030; 
				elsif(BGACK30_Q = '1' and BGACK30 = '1') then -- end of 030 cycle
					DMA_SM <= WAIT_FOR_BGACK;
				else
					DMA_SM <= WAIT_FOR_BGACK_FINISH; --030-Bus cycle running
				end if;
			when PRE_BUS_MASTER_040 => --MC DH State 7
				BG40_SIG <= '0';
				DMA_SM <= BUS_MASTER_040;				
			when BUS_MASTER_040 => --MC DH State 8
				-- This is the main 68040 as master running state. As long
				-- as the '040 wants the bus and the '030 doesn't, stay
				-- here. 
				BG40_SIG <= '0';
				if (BR30_Q='0' and BR30 = '0') then -- BR030: go to busbusy test
					DMA_SM <= RELEASE_BG040; 
				else
					DMA_SM <= BUS_MASTER_040; -- stay here
				end if;
			when RELEASE_BG040 => --MC DH State 9
				-- At this point we drop grant to the '040, and would like
				-- to let the '030 on the bus.  
				-- We need this transit state to let the 040 recognize, that it lost bus ownership
				DMA_SM <= WAIT_BUS_FREE_040;
			when WAIT_BUS_FREE_040 =>
				-- If the '040 has dropped the bus, it's ok to proceed.  
				-- If not, either hang out here as long as bus busy is 
				-- asserted and the 040 is not starting a new locked cycle.  
				-- If it is, go back to the running state.
				if(BB40_Q = "11" and BB40 = '1') then --040- bus not busy: go to BG30
					DMA_SM <= ARBIT_BUS;
				else
				--elsif(BR40_Q='1')then -- no 040 request: wait another round
					DMA_SM <= WAIT_BUS_FREE_040;					
				--else
				--	DMA_SM <= BUS_MASTER_040; -- a new 040 bus cycle started, probably a LOCKED cycle
				end if;
			when WAIT_BUS_FREE_040_2 =>
				if(BB40_Q = "11" and BB40 = '1') then --040- bus not busy: go to BG30
					DMA_SM <= WAIT_BUS_FREE_040;			
				else
					DMA_SM <= WAIT_BUS_FREE_040_2;				
				end if;			
			end case;			
		end if;
	end process DMA_ARBIT;
	
	--DMA 030-termination sync
	process (CLK30,RESET30)
	begin
		if(RESET30 = '0')then			
			STERM_DMA_30 <= '1';
			--AS_D <= "11111";
		elsif(rising_edge(CLK30)) then
--			AS_D <= AS_D(3 downto 0) & AS30;
--			if(AS_D(4)='0' and BGACK30 = '0') then
--				STERM_DMA_30 <= STERM_DMA; -- we need this signal to stabilize for at least half a clock
--			elsif(AS30='1' or BGACK30='1') then
--				STERM_DMA_30 <= '1';
--				AS_D <= "11111";
--			end if;
--			if(AS30 = '0' and BGACK30 = '0') then
				STERM_DMA_30 <= STERM_DMA; -- we need this signal to stabilize for at least half a clock
--			else
--				STERM_DMA_30 <= '1';
--			end if;
		end if;
	end process;


	-- regulat bus access-stuff
	
	--on a 68030 the interesting parts mainly happen on the falling cpu-clocks
	MC68030_N_SM: process (RESET30, CLK30)
	begin
		if(RESET30 ='0') then
			AS30_SIG <= '1';
			DS30_SIG <= '1';
			SM030_N <= S1;	
			SIZING <= size_decode;
			LE_BS_SIG_N <='0';
			START_ACK <= '0';
			END_SEND <= '0';
		elsif(falling_edge(CLK30)) then
			--default values
			AS30_SIG <= '1';
			DS30_SIG <= '1';			
			LE_BS_SIG_N <= '0';
			case SM030_N is
			when S1 =>					
				if( START_ACK /= START_SEND_SAMPLED) then
					START_ACK <= START_SEND_SAMPLED; --update internal value after sync with start of cycle
					AS30_SIG <= '0';
					DS30_SIG <= not RW30_Q;
					--LE_BS_SIG_N <= not RW30_Q;
					SM030_N <= S3;	
				elsif(SIZING/=size_decode) then -- another cylce for this round
					AS30_SIG <= '0';
					DS30_SIG <= not RW30_Q;
					--LE_BS_SIG_N <= not RW30_Q;
					SM030_N <= S3;
				else
					SM030_N <= S1;
				end if;	
			when S3 =>
				AS30_SIG <= '0';
				DS30_SIG <= '0';
				--LE_BS_SIG_N <= not RW30_Q;
				if(	DSACK30 < "11"
					or STERM30 ='0' 
					or BERR30 ='0'					  					
					) then
					SM030_N <=S5;
				else
					SM030_N <=S3;
				end if;				
			when S5 =>
				LE_BS_SIG_N <= RW30_Q;
				SM030_N <=S1;
				--target for next bussizing-cycle
				case SIZING is				
					when size_decode =>	
						-- first cycle completed, see how wide the bus was!
						-- the buslength can not change during one cycle!
						if(STERM30 = '0' or BERR30 = '0')then --32bit term or error->end!
							SIZING 		<= size_decode;
							END_SEND	<= not END_SEND; -- toggle END_SEND to indicate end of cycle
						elsif( 	(DSACK_TERM="10" and LONG = '1') or				-- BYTETERM and LONG
								(DSACK_TERM="10" and WORD = '1' and A40(1)='0')	-- BYTETERM and even WORD: A(1)=0
							)then
							SIZING 		<= get_byte2;
						elsif( 	DSACK_TERM="10" and WORD = '1' and A40(1)='1'		-- BYTETERM and odd WORD: A(1) = 1
							) then
							SIZING 		<= get_byte0;
						elsif( 	DSACK_TERM="01" and LONG = '1'						-- WORDTERM and LONG access = get lower word
							) then
							SIZING 		<= get_low_word;							
						else -- all other cases (word access on word term, byte access on byteterm/wordterm): Fine
							SIZING 		<= size_decode;
							END_SEND	<= not END_SEND; -- toggle END_SEND to indicate end of cycle
						end if;
					when get_low_word =>	
						-- must be WORDTERM so its finished!
						SIZING 		<= size_decode;
						END_SEND	<= not END_SEND; -- toggle END_SEND to indicate end of cycle
					when get_byte2 =>
						if(	WORD = '1' ) then									-- BYTETERM and WORD length -> finished 
							SIZING 		<= size_decode;
							END_SEND	<= not END_SEND; -- toggle END_SEND to indicate end of cycle
						else													-- BYTETERM and LONG -> get byte 1								
							SIZING <= get_byte1;
						end if;
					when get_byte1 =>
						SIZING 		<= get_byte0;								-- BYTETERM and LONG -> get byte 0	
					when get_byte0 =>
						SIZING 		<= size_decode;								-- BYTETERM and LONG length -> finished 
						END_SEND	<= not END_SEND; -- toggle END_SEND to indicate end of cycle
				end case;					 
			end case;
		end if;		
	end process MC68030_N_SM;

	--The positive clock statemachine
	MC68030_P_SM: process (CLK30,RESET30) begin
		if (RESET30 ='0') then
			SM030_P <= S0;
			LE_BS_SIG_P <= '0';
			START_SEND_SAMPLED <= '0';
		elsif (rising_edge(CLK30)) then
			LE_BS_SIG_P <= '0';
			START_SEND_SAMPLED <= START_SEND; -- we need this signal to be stable for at least one half clock!
			case SM030_P is
			when S0 =>				
				if(SM030_N=S3) then --negative statemachine advanced from S1 to S3
					LE_BS_SIG_P <= not RW30_Q; --latch data from 68040-cpu to bussizing cpld
					SM030_P <= S2;			
				end if;
			when S2 =>
				if(SM030_N=S5) then --negative statemachine advanced from S3 to S5
					SM030_P <= S4;
				end if;
			when S4 =>			
				SM030_P <= S0; --automatically advance to S0
			end case;		
		end if;
	end process MC68030_P_SM;	
		
	--the bussize, size, rw and FC control
	BS_LATCH: process(CLK30,RESET30)begin
		if (RESET30 ='0') then
			BWL_BS_D <= "111";
			AL_D <= "11";
			SIZ30_D <= "11";
			RW30_Q <= '1';
			FC30_D <= "111";
			DSACK_TERM <="11";
			AS30_D_C <= '1';
		elsif (rising_edge(CLK30)) then
			AS30_D_C <= AS30;
			if(BGACK30 ='1')then
				
				--buffer this signal to calculate the bus termination width
				if(STERM30='0' or BERR30='0')then
					DSACK_TERM <="00";
				else
					DSACK_TERM <= DSACK30; 
				end if;
				
				if( (TT40(1)='0' AND (TM40(1)='0' or TM40(1 downto 0)="11")) or	-- user data
					(TT40(1 downto 0 )="10" AND TM40(0)='1') or					-- alt logical func
					(TT40(1 downto 0 )="11"))then								-- avec / breakpoints
					FC30_D(0)	<= '1';
				else
					FC30_D(0)	<= '0';
				end if;

				if( (TT40(1)='0' AND TM40(1 downto 0)="10") or					-- supv / user code
					(TT40(1 downto 0)="10" AND TM40(1)='1') or					-- alt logical func
					(TT40(1 downto 0 )="11"))then								-- avec / breakpoints
					FC30_D(1)	<= '1';
				else
					FC30_D(1)	<= '0';
				end if;
						
				if( (TT40(1)='0' AND TM40(2 downto 0)="101") or					-- supv data
					(TT40(1)='0' AND TM40(2 downto 0)="110") or 				-- supv code
					(TT40(1 downto 0 )="10" AND TM40(2)='1') or					-- alt logical func
					(TT40(1 downto 0 )="11"))then								-- avec / breakpoints
					FC30_D(2)	<= '1';
				else
					FC30_D(2)	<= '0';
				end if;
				
				

				case SIZING is
				when size_decode =>
					--RW store for the whole cycle
					RW30_Q <= RW40;
					
					--size and address decode
					SIZ30_D(0)	<= BYTE;
					SIZ30_D(1)	<= WORD;								
					if(LONG = '0') then
						AL_D <=	A40;
					else
						AL_D <= "00";
					end if;						

					--bus code for data latch
					if(	(RW40 ='0' and BYTE = '1' and A40(0)='1')  	-- WRITE: byte acces on odd address
						) then 						
						BWL_BS_D(0)	<=	'1';
					else 
						BWL_BS_D(0)	<=	'0';
					end if;
						
					if(	(RW40 ='0' and ( LONG = '1' OR							-- WRITE: LONG
							(WORD = '1' and A40(1)='0')	or							-- WRITE: WORD A1=0
							(BYTE = '1' and A40(1)='0')))or 							-- WRITE: BYTE A1=0
							(RW40 ='1' and (DSACK30="01" or DSACK30="10"))) then -- READ: word/byte port
						BWL_BS_D(1)	<=	'0';
					else 
						BWL_BS_D(1)	<=	'1';
					end if;

					if(	(RW40 ='0' and BYTE = '0') or 								-- WRITE: LONG/WORD access
							(RW40 ='1' and DSACK30="10")) then 						-- READ: byte port
						BWL_BS_D(2)	<=	'0';
					else 
						BWL_BS_D(2)	<=	'1';
					end if;
				when get_low_word =>
					--size and address decode
					SIZ30_D	<= "10";
					AL_D	<= "10";
					--bus code for data latch
					if(RW40='0') then
						BWL_BS_D(2 downto 0) <= "010";
					else
						BWL_BS_D(2 downto 0) <= "101";
					end if;
				when get_byte2 =>
					--size and address decode
					SIZ30_D	<= "01";
					AL_D	<= "01";
					--bus code for data latch					
					BWL_BS_D(2 downto 0) <= "001";
				when get_byte1 =>
					--size and address decode
					SIZ30_D	<= "01";
					AL_D	<= "10";
					--bus code for data latch
					BWL_BS_D(2 downto 0) <= "010";
				when get_byte0 =>
					--size and address decode
					SIZ30_D	<= "01";
					AL_D	<= "11";
					--bus code for data latch
					BWL_BS_D(2 downto 0) <= "011";				
				end case;
			else --dma access
				if(AS30_D_C = '1' and AS30 = '0')then -- hold this stable over the whole cycle, the timing error does not matter
					RW30_Q <= RW30;
					SIZ30_D <= SIZ30;
					AL_D <= AL;
					if(RW30 ='1')then
						BWL_BS_D <= "000"; -- a DMA READ is a TK write on the 030 bus
					else
						BWL_BS_D <= "110"; -- a DMA WRITE is a TK read on the 030 bus -- always 32 bit!
					end if;
				end if;
			end if;
		end if;	
	end process;

end Behavioral;

