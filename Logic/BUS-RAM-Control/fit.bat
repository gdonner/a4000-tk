SET PATH=%PATH%;c:\Xilinx_ISE\14.7\ISE_DS\ISE\bin\nt64;c:\Xilinx_ISE\14.7\ISE_DS\ISE\lib\nt64

cpldfit.exe -intstyle ise -p xc95288xl-10-TQ144 -ofmt vhdl -htmlrpt -optimize speed -loc on -slew fast -init low -inputs %1 -pterms %2 -unused float -power std -terminate float mcp.ngd >log.txt
@IF %ERRORLEVEL% NEQ 0 GOTO NO_SUCCESS
tsim -intstyle ise mcp mcp.nga
hprep6 -s IEEE1149 -n mcp -i mcp
GOTO END
:NO_SUCCESS
@echo !!!!
@echo error fitting the jed!
@echo !!!!
:END