----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:13:29 01/26/2020 
-- Design Name: 
-- Module Name:    mcp - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mcp is
    Port ( A : in  STD_LOGIC_VECTOR (31 downto 2);
           RAM_A : out  STD_LOGIC_VECTOR (12 downto 0);
           IPL030 : in  STD_LOGIC_VECTOR (2 downto 0);
           IPL040 : out  STD_LOGIC_VECTOR (2 downto 0);
           BWL_BS : out  STD_LOGIC_VECTOR (2 downto 0);
           SIZ030 : inout  STD_LOGIC_VECTOR (1 downto 0);
           SIZ040 : inout  STD_LOGIC_VECTOR (1 downto 0);
           A040 : inout  STD_LOGIC_VECTOR (1 downto 0);
           A030 : inout  STD_LOGIC_VECTOR (1 downto 0);
           TEA040 : out  STD_LOGIC;
           ROM : out  STD_LOGIC;
           FC030 : inout  STD_LOGIC_VECTOR (2 downto 0);
           RW030 : inout  STD_LOGIC;
           BGACK030 : in  STD_LOGIC;
           RSTIN040 : out  STD_LOGIC;
           PLL_CLK : in  STD_LOGIC;
           SYSTEM_CLK : in  STD_LOGIC;
           DS030 : inout  STD_LOGIC;
           BG030 : out  STD_LOGIC;
           AVEC030 : in  STD_LOGIC;
           AS030 : inout  STD_LOGIC;
           LE_BS : out  STD_LOGIC;
           TBI040 : out  STD_LOGIC;
           AVEC040 : out  STD_LOGIC;
           OE_BS : out  STD_LOGIC;
           RAM_DQ : out  STD_LOGIC_VECTOR (3 downto 0);
           RAM_WE : out  STD_LOGIC;
           RAM_CAS : out  STD_LOGIC;
           RAM_RAS : out  STD_LOGIC;
           RAM_CE : out  STD_LOGIC_VECTOR (1 downto 0);
           RAM_BA : out  STD_LOGIC_VECTOR (1 downto 0);
           OE_RAM_040 : out  STD_LOGIC;
           LE_RAM_040 : out  STD_LOGIC;
           OE_040_RAM : out  STD_LOGIC;
           RAM_CLK : out  STD_LOGIC;
           RAM_CLK_EN : out  STD_LOGIC;
           TT040 : inout  STD_LOGIC_VECTOR (1 downto 0);
           RSTO040 : in  STD_LOGIC;
           BCLK : out  STD_LOGIC;
           TM040 : inout  STD_LOGIC_VECTOR (2 downto 0);
           BCLK_EN : out  STD_LOGIC;
           RW040 : inout  STD_LOGIC;
           SNOOP060 : out  STD_LOGIC;
           CLK_040 : out  STD_LOGIC;
           TCI040 : out  STD_LOGIC;
           BG040 : out  STD_LOGIC;
           TS040 : inout  STD_LOGIC;
           TA040 : inout  STD_LOGIC;
           BB040 : inout  STD_LOGIC;
           BR040 : in  STD_LOGIC;
           BR030 : in  STD_LOGIC;
           STERM030 : inout  STD_LOGIC;
           BERR030 : in  STD_LOGIC;
           RESET030 : inout  STD_LOGIC;
		   DSACK030 : inout  STD_LOGIC_VECTOR (1 downto 0));
end mcp;

architecture Behavioral of mcp is
 -- the ramcontroller

	COMPONENT ramcon 
	Port (
		CLK_RAMC : in  STD_LOGIC;		
		RESET : in std_logic;
		C25MHZ : in std_logic;
		RW_40 : in std_logic;
		TT40_1 : in std_logic;
		TS40: in std_logic;
		AS30: in std_logic;
		A40		: in STD_LOGIC_VECTOR (31 downto 0);
		SIZ40		: in STD_LOGIC_VECTOR (1 downto 0);
		DQ: out STD_LOGIC_VECTOR (3 downto 0);
		WE: out std_logic;
		CAS: out std_logic;
		RAS: out std_logic;
		CLK_RAM: out std_logic;
		CLKEN: out std_logic;
		LE_RAM: out std_logic;
		OERAM_40: out std_logic;
		OE40_RAM: out std_logic;
		CE 		: out STD_LOGIC_VECTOR (1 downto 0);      
		BA			: out STD_LOGIC_VECTOR (1 downto 0);      
		ARAM		: out STD_LOGIC_VECTOR (12 downto 0);      
		RAM_ACCESS: out std_logic;
		CACHE_INHIBIT: out std_logic;
		TA_RAM: out std_logic
	);
    END COMPONENT;
	
	COMPONENT control 
	Port (
			  --clocks
			  CLK30 : in  STD_LOGIC;								--CLK from 68030-Bus
			  BUS_CLK : in STD_LOGIC;								--Bus CLK from 040-CPU
			  
			  --Reset
			  RSTO40 : in  STD_LOGIC;								--Reset-out from 040-CPU 
			  RSTI40 : out  STD_LOGIC;								--Reset-in from 040-CPU
			  RESET30 : inout  STD_LOGIC;							--Reset from/to 68030-Bus
			  
			  --68030-BUS
			  AL	: inout STD_LOGIC_VECTOR (1 downto 0);			--Address A0-1 to 68030-Bus
			  AS30 : inout  STD_LOGIC;								--Address-Strobe to 68030-Bus
			  DS30 : inout  STD_LOGIC;								--Data-Strobe to 68030-Bus
			  RW30 : inout  STD_LOGIC;								--Read/Write to 68030-Bus
			  FC30 : out  STD_LOGIC_VECTOR (2 downto 0);			--Function-code 0-2 to 68030-Bus
			  SIZ30 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer size bits 0-1 to 68030-Bus
			  IPL30 : in  STD_LOGIC_VECTOR (2 downto 0);			--Interrupt Prio-Level 0-2 from 68030-Bus
			  DSACK30 : in  STD_LOGIC_VECTOR (1 downto 0);			--DSACK Bit 0/1 from 68030-Bus
			  DSACK30out : out  STD_LOGIC;							--DMA-Cycle end out to 68030-Bus
			  STERM30 : in  STD_LOGIC;								--STERM from 68030-Bus
			  BERR30 : in  STD_LOGIC;								--Bus-Error from 68030-Bus
			  AVEC30 : in  STD_LOGIC;								--Cache-Inhibit from 68030-Bus
			  DMA_CYCLE :out STD_LOGIC;								--DMA-cycle flag to RAM-controller
			  
			  --68040-BUS
			  A40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Address 0-1 from 040-CPU
			  TS40 : inout  STD_LOGIC;								--Transfer-Start from 040-CPU
			  RW40 : inout  STD_LOGIC;								--Read/Write from 040-CPU
			  TM40 : inout  STD_LOGIC_VECTOR (2 downto 0);			--Transfer Modifier Bit 0-3 from 040-CPU
			  TT40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer-Type Bit 0-1 from 040-CPU
			  SIZ40 : inout  STD_LOGIC_VECTOR (1 downto 0);			--Transfer-Breite Bit 0-1 from 040-CPU
			  IPL40 : out  STD_LOGIC_VECTOR (2 downto 0);			--Interrupt Prio-Level 0-2 to 040-CPU
			  TBI40 : out  STD_LOGIC;								--Transfer Burst Inhibit to 040-CPU
			  TCI40 : out  STD_LOGIC;								--Transfer Cache Inhibit to 040-CPU
			  TA40 : inout  STD_LOGIC;								--Transfer Acknowledge to 040-CPU
			  TEA40 : out  STD_LOGIC;								--Transfer Error Acknowledge to 040-CPU
			  AVEC40 : out  STD_LOGIC;								--Autovectorcycle to 040-CPU
			  
			  --RAM
			  RAM_ACCESS : in  STD_LOGIC;							--RAM_ACCESS indicator
			  TA_RAM : in  STD_LOGIC;								--Transfer Acknowledge from RAM controller		    
			  CACHE_INHIBIT: in std_logic;							--Cache controll
			  
			  --Bus Sizing
			  OE_BS : out  STD_LOGIC;								--Output-Enable for 68030-68040 bus-sizing
			  LE_BS : out  STD_LOGIC;								--Latch-Enable for 68030-68040 bus-sizing
			  BWL_BS : out  STD_LOGIC_VECTOR (2 downto 0);			--BYTE/WORD/LONG Bus-Sizing Bit 0-3

			  --DMA
			  BR30 :  in  STD_LOGIC;								--Busrequest 68030-Bus
			  BGACK30 :  in  STD_LOGIC;								--Bus Grant ack 68030-Bus		  
			  BG30 : out  STD_LOGIC;								--Busgrant 68030-Bus
			  BR40 :  in  STD_LOGIC;								--Busrequest 68040-Bus
			  BG40 :  out  STD_LOGIC;								--Busgrant 68040-Bus
			  BB40 :  inout  STD_LOGIC;								--Busbusy 68040-Bus
			  SNOOP60 : out  STD_LOGIC								--Bus-Snoop-operation for DMA invalidation (68060 only)
	);
    END COMPONENT;	
	signal BUS_EN_SIG : STD_LOGIC :='1';
	signal RAM_ACCESS : STD_LOGIC :='1';
	signal TA_RAM : STD_LOGIC :='1';	
	signal ROM_ACCESS : STD_LOGIC_VECTOR (1 downto 0);
	signal A40_RAM : STD_LOGIC_VECTOR (31 downto 0);
	signal DSACK_INT : STD_LOGIC_VECTOR (1 downto 0);
	signal STERM_INT : STD_LOGIC;	
	signal DMA_CYCLE : STD_LOGIC :='0';
	signal CACHE_INHIBIT : STD_LOGIC :='0';
	signal DSACK30out : STD_LOGIC;	
	signal PLL_CLK_HALF : STD_LOGIC:='0';
	signal PCLK_SIG : STD_LOGIC:='0';	
	signal BCLK_SIG : STD_LOGIC:='0';	
	signal MCLK_SIG : STD_LOGIC:='0';
begin

	A40_RAM <= A(31 downto 2) & A040(1 downto 0);

	RAM: ramcon PORT MAP (
		CLK_RAMC => MCLK_SIG,
		RESET =>RESET030,
		C25MHZ =>SYSTEM_CLK,
		RW_40 =>RW040,
		TT40_1 =>TT040(1),
		TS40 =>TS040,
		AS30 => DMA_CYCLE,
		A40	=>A40_RAM,
		SIZ40 =>SIZ040,
		DQ =>RAM_DQ,
		WE 	=>RAM_WE,
		CAS =>RAM_CAS,
		RAS	=>RAM_RAS ,
		CLK_RAM=> RAM_CLK,
		CLKEN=> RAM_CLK_EN,
		LE_RAM=> LE_RAM_040,
		OERAM_40=> OE_RAM_040,
		OE40_RAM=> OE_040_RAM,
		CE 	=> RAM_CE,      
		BA	=> RAM_BA,      
		ARAM => RAM_A,     
		RAM_ACCESS=> RAM_ACCESS,
		CACHE_INHIBIT =>CACHE_INHIBIT,
		TA_RAM=> TA_RAM
		);
		
	BUS_CONTROL: control Port MAP(
			  --Takte
			  CLK30 => SYSTEM_CLK,
			  BUS_CLK => BCLK_SIG,
			  
			  --Reset
			  RSTO40 => RSTO040,
			  RSTI40 => RSTIN040,
			  RESET30 => RESET030,
			  
			  --68030-BUS
			  AL => A030,
			  AS30 => AS030,
			  DS30 => DS030,
			  RW30 => RW030,
			  FC30 => FC030,
			  SIZ30 => SIZ030,
			  IPL30 => IPL030,
			  DSACK30 => DSACK_INT,
			  DSACK30out => DSACK30out,
			  STERM30 => STERM_INT,
			  BERR30 => BERR030,
			  AVEC30 => AVEC030,
			  DMA_CYCLE => DMA_CYCLE,
			  
			  --68040-BUS
			  A40 => A040,
			  TS40 => TS040,
			  RW40 => RW040,
			  TM40 => TM040,
			  TT40 => TT040,
			  SIZ40 => SIZ040,
			  IPL40 => IPL040,
			  TBI40 => TBI040,
			  TCI40 => TCI040,
			  TA40 => TA040,
			  TEA40 => TEA040,
			  AVEC40 => AVEC040,
			  
			  --RAM
			  RAM_ACCESS => RAM_ACCESS,	  
			  TA_RAM	=> TA_RAM,
			  CACHE_INHIBIT => CACHE_INHIBIT,
			  
			  --Bus Sizing
			  OE_BS => OE_BS,
			  LE_BS => LE_BS,
			  BWL_BS => BWL_BS,

			  --DMA
			  BR30 => BR030,
			  BGACK30 => BGACK030,	  
			  BG30 => BG030,
			  BR40 => BR040,
			  BG40 => BG040,
			  BB40 => BB040,
			  SNOOP60 => SNOOP060
	);	
	--clocks
	PCLK_SIG <= not PLL_CLK; --CPU Clock
	BCLK_SIG <= not PLL_CLK; --BUS-CLOCK
	MCLK_SIG <= not PLL_CLK; --Memory Clock
	BUS_EN_SIG <= '0';

	CLK_040  <= PCLK_SIG;
	BCLK	 <= BCLK_SIG;
	BCLK_EN  <= BUS_EN_SIG;

	process(PLL_CLK) begin
		if(falling_edge(PLL_CLK)) then
			PLL_CLK_HALF <= not PLL_CLK_HALF;
		end if;
	end process;

	
	--ROM decode
	process(SYSTEM_CLK) begin
		if(rising_edge(SYSTEM_CLK)) then
			-- I need to hold this signal a bit to make the Bus-sizing recognize the last DSACK.
			-- DSACK has to go away one half clock after AS030 goes away. So this is fine
			if(AS030 ='0' and (A(31 downto 19))= (x"00F"&'0') and BGACK030 ='1')then --$00F00000- $00F7FFFF
				ROM_ACCESS(0)<='0';
				ROM_ACCESS(1)<= ROM_ACCESS(0);
			else
				ROM_ACCESS <= "11";
			end if;
		end if;
	end process;
	
	ROM <= '0' when (ROM_ACCESS(0) ='0' ) else '1';
	--DSACK gen
	DSACK_INT <= DSACK030 when ROM_ACCESS(0)='1' else
				 "10" when ROM_ACCESS(1)='0' else 
				 "11";
	STERM_INT  <= STERM030 when ROM_ACCESS(0)='1' else '1';			 
				 
	--use DSACK to terminate a DMA-cycle
	DSACK030 <= (DSACK30out & DSACK30out) when RAM_ACCESS ='0' and BGACK030='0' else "ZZ";
	STERM030 <=	'1' when RAM_ACCESS ='0' and BGACK030='0' else 'Z';
	
	--use STERM to terminate a DMA-cycle
	--DSACK030 <= "11" when RAM_ACCESS ='0' and BGACK030='0' else "ZZ";
	--STERM030 <= DSACK30out when RAM_ACCESS ='0' and BGACK030='0' else 'Z';
end Behavioral;

