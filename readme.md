# 68060 Turbocard for Amiga A3000(T) and A4000(T)
 
This is a turbocard for for Amiga A3000 and A4000(T) with up to 128MB SDRAM, an 68060CPU running at 50/75/100MHz even a 68040 on 25MHz. 

The RAM is in the Amiga CPU-ram space and therefore autodetect.

The RAM can handle move16-bursts and DMA from the Amiga

One nice feature is the LTC2990-voltage and temperature monitor, which can be accessed via an I²C board, e.g. IcyBoard.

For now the card does NOT support vectored interrupts like the A4091 is supposed to use.

The A3000T is very picky and I couldn't test it, because I don't have one. It SHOULD work, but don't blame me if it doesn't! Even the A3640 is hardly working in an A3000T :(

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA/HARDWARE!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
The board consists of nine functional groups:
1. The CPU: It can be a  68040 or a 68060. Since the later runs on 3.3V you have to switch the core-voltage via two ferrite beads. Populate L2/L4 for a 68060 and L3/L5 for a 68040. The CPUs need different firmwares. The 68040 is found in the 68040-branch of this repository.
2. The bootrom: It disables the 68060-FPU during startup to be compatible with Kick 2.0/3.0/3.1. Additionally it adds a virtual expansion entry (ID 2588/60) to the expansion board menue. This entry does nothing, but show off ;). Last but not least it displays some nice colorbars at startup to show, that the board is basicly working.
3. A DCDC-converter 5V/3.3V@6A. This module helps to keep the design simple and makes a more efficent conversion form 5V to 3.3V than an ordinary linear voltage regulator.
4. A PLL-IC to generate a phase-synced multiplication of the 25MHz-clock. The multiplier is controlled via the small 2mm-jumpers on JP2: S0=1 S1:0 is 100MHz, S0:open, S1:0 is 75MHz (unstable at the moment), S0:1 S1:1 is 50Mhz, S0:0 S1:1 is 25MHz
6. The optional LM2990-voltage meter, wich has to be hooked to an EXTERNAL I²C-controller via the two jumper wires SCL/SCA
7. Two 3.3V/5V levelshifters between the CPU and the RAM
8. Four 16bit-SDRAM ICs (up to 128MB)
9. The two CPLDs holding the whole logic:

The smaller CPLD is a giant latch/multiplexer and stores the intermediate results of the bussizing translation. Remember: The 68020/030 features automatic bussizing, the 68040/68060 not! So you have to translate an 8-Bit port (the bootrom) and a 16-Bit port (the basic Amiga chipset/Z2) to a 32 wide long port (the 68040/060 CPU). 

The bigger CPLD holds the SD-RAM-controller, the statemachine for the 68030/060-translation and the statemachine for the DMA-translation and some glue logic.

There are several solder jumpers on the board:

SJ_A3/SJ_A4 should be soldered to the left for now. (potential vectored interrupt support)

The solder jumper labeled "ICS570-supply" should be soldered to the left if an ICS570B (3.3V) is used and to the right if a n ICS570A (5V obsolete) is used. Please check the correct voltage via a multimeter before you power up the system!

Near the IC12 (LTC2990) are two jumpers, which change the I²C-address of the voltage monitor. It is suggested to solder both to their left (1) position. The associated address 0x9e is directly supported by available software and won't collide with other existing applications using the LTC2990. A tool that can display voltages and temperatures is the free [Sensei](https://aminet.net/package/driver/other/Sensei).

To read the temperature of the 68060 (the 68040 does not have this capability) directly via the integrated thermal resistor of the CPU, you'll have to solder four pins at JP3 and connect them with two jumpers following the arrow markings on the board. The resistor values can vary a lot between CPUs, so you should calibrate Sensei by temporarily removing the jumpers and measuring resistance between the two JP3 pins closest to the board's connector. Note both this value and the room temperature near the card  after letting it cool down overnight. Refer to Sensei's documentation to enter the calibration values into its configuration.

The JTAG right to the CPU is the CPU-JTAG and doesn't need to be populated. Its only interesting, if you want to debug the CPU via JTAG. The JTAG is enabled via the optional jumper CPU_JTAG.

## What is in this repository?
This repository consists of several directories:
* BFGFlash: An AmigaOS utility to put BootROM and Kickstart modules into F0-ROM
* Bootrom: The bootcode to put into F0-ROM to boot the machine 
* PCB: The board, schematic and bill of material (BOM) are in this directory. 
* Logic: The VHDL-Sources, pinlists and ISE-project files for the two CPLDs are in this directory. 
* Pics: Pictures

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. I recoment WINBOND W9825G6KH-5 for SDRAM. Alliance and ISSI works too, but it must be at least 6ns! 
The size of an 5V fan is critical in an A3000! I recomend ADDA AP4505MX-G90 (5V) or ADDA AP0412MX-J70 (12V also works with reduced speed on 5V). 
The DC/DC powerconverter is an OKX-T/5-W5P-C from Murata, Digikey number 811-3326-ND. As ROM any JED-compatible flash can be used: 29F010, 29F040, 39SF010(A),39SF040 and many more. Check the pinout and compare it to an 29F040. All FlashROMs are more or less the same. Of course, if you choose an 29F010 or similar you have only 128KB of flash memory, whch could be used for additional modules in the BootROM (like BlitzKick). However, I'm working on write support and then only a limited number of FlashROMs might be supported for that. 

## How to programm the board?
The CPLD must be programmed via Xilinx IMPACT and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/index.php?threads/73349/). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

## How do I install it in an Amiga?
Hardware:
Disassemble your Amiga carefully, remove any existing CPU-card from the LocalSlot ant put this card in. Reassemble the Amiga and you are ready to boot!

Software:
To get thze best performance, you have to install the (mmu.library)(https://aminet.net/package/util/libs/MMULib). And add the following line to the MMU-Configuration found in ENVARC:MMU-configuration:

SetCacheMode from 0x08000000 size 0x08000000 Valid CopyBack

**ATTENTION: Caching of Z3 is now enabled!** 
RAM-Cards, like BigRam ort Z3-Fastlane could not be cached and lost a lot of performance. You can enable caching for them via mmu.library now. 
If you have Z3-IO cards you want to disable caching.

MMU-library configuration works like this:

Enable caching:
For <Manufactor ID> <product id>  SetCacheMode {base}     {size}      Valid CopyBack

Disable caching:
For <Manufactor ID> <product id>  SetCacheMode {base}     {size}      CacheInhibit NonSerial Imprecise

A working MMU-Configuration is provided in this folder. 

To write some additional drivers into the flash, you have to copy the program BFGFlash/FlashBFGCLI into C: and you can put any driver you want into the ROM (eg icon.library and workbench.library )
