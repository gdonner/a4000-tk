; Bigbox Amiga reset code
; (C) 2022 Henryk Richter
;
; This is an old example method that can also be found 
; in the german edition of the classic Amiga Intern book.
; 
;
	XDEF	_KBDReset
	XDEF	_KBDR_Blink

	mc68040



_KBDReset:
	move.w	#$4000,$DFF09A	;disable Interrupts
	lea	$bfe001,a0
	move.b	#$1F,$d00(a0)	;icr:  clear all interrupts
	move.b	#$44,$e00(a0)	;cra:  OUTMODE=TOGGLE,SPMODE=output
	move.b	#$ff,$400(a0)	;talo: max. timer run length
	move.b	#$ff,$500(a0)	;tahi: max. timer run length
	move.b	#$00,$c00(a0)	;sdr:  serial data register = 0 (i.e. KBDAT=0)
	move.b	#$55,$e00(a0)	;cra:  OUTMODE=TOGGLE,SPMODE=output,LOAD,START

.kbdreset_wait:			;waiting to die here...
	btst	#0,$d00(a0)	;icr: poll Timer A
	beq.s	.kbdreset_wait

	;CAUTION! 68040+ only, enter in SuperState()

	; didn't work ? Try something else (2.0+)
	; note: good practice would suggest to disable Caches, MMU, TTR, DMA
	movec.l	vbr,a0			;
	move.l	#supfunc,$20(a0)	;priv trap
	cinva	bc			;
	or.w	#$2000,sr		;
supfunc:
	move.l	$f80004,a0	;get ColdStart pointer
	subq.l	#2,a0		;RESET instruction sits before ColdStart
	jmp	(a0)
	rts

; RED pattern on COLOR0
_KBDR_Blink:
	 moveq	#-1,d1
	 moveq	#-1,d0
.wait2:
.wait:
	 lsl.l	#8,d0			; red only
	 move.w	d0,$dff180
	 lsr.l	#8,d0
	 tst.b	$BFE001
	 dbf	d0,.wait		; $ffff.w
	 dbf	d1,.wait2
	rts

