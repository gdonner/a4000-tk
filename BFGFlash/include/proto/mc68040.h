#ifndef _PROTO_MC68040_H
#define _PROTO_MC68040_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_MC68040_PROTOS_H) && !defined(__GNUC__)
#include <clib/mc68040_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *MC68040Base;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/mc68040.h>
#else
#include <inline/mc68040.h>
#endif
#elif !defined(__VBCC__)
#include <pragma/mc68040_lib.h>
#endif

#endif	/*  _PROTO_MC68040_H  */
