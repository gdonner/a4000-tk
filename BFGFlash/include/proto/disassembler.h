#ifndef _PROTO_DISASSEMBLER_H
#define _PROTO_DISASSEMBLER_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_DISASSEMBLER_PROTOS_H) && !defined(__GNUC__)
#include <clib/disassembler_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *DisassemblerBase;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/disassembler.h>
#else
#include <inline/disassembler.h>
#endif
#elif !defined(__VBCC__)
#include <pragma/disassembler_lib.h>
#endif

#endif	/*  _PROTO_DISASSEMBLER_H  */
