#ifndef _INLINE_MC680X0_H
#define _INLINE_MC680X0_H

#ifndef CLIB_MC680X0_PROTOS_H
#define CLIB_MC680X0_PROTOS_H
#endif

#ifndef  EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef  LIBRARIES_680X0_H
#include <libraries/680x0.h>
#endif

#ifndef MC680X0_BASE_NAME
#define MC680X0_BASE_NAME MC680x0Base
#endif

#define CPUType() ({ \
  register char * _CPUType__bn __asm("a6") = (char *) (MC680X0_BASE_NAME);\
  ((char (*)(char * __asm("a6"))) \
  (_CPUType__bn - 30))(_CPUType__bn); \
})

#define FPUType() ({ \
  register char * _FPUType__bn __asm("a6") = (char *) (MC680X0_BASE_NAME);\
  ((char (*)(char * __asm("a6"))) \
  (_FPUType__bn - 36))(_FPUType__bn); \
})

#define MMUType() ({ \
  register char * _MMUType__bn __asm("a6") = (char *) (MC680X0_BASE_NAME);\
  ((char (*)(char * __asm("a6"))) \
  (_MMUType__bn - 42))(_MMUType__bn); \
})

#define SetFPUExceptions(flags, mask) ({ \
  ULONG _SetFPUExceptions_flags = (flags); \
  ULONG _SetFPUExceptions_mask = (mask); \
  ({ \
  register char * _SetFPUExceptions__bn __asm("a6") = (char *) (MC680X0_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), ULONG __asm("d0"), ULONG __asm("d1"))) \
  (_SetFPUExceptions__bn - 48))(_SetFPUExceptions__bn, _SetFPUExceptions_flags, _SetFPUExceptions_mask); \
});})

#endif /*  _INLINE_MC680X0_H  */
