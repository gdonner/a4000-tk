#ifndef _INLINE_MMU_H
#define _INLINE_MMU_H

#ifndef CLIB_MMU_PROTOS_H
#define CLIB_MMU_PROTOS_H
#endif

#ifndef  EXEC_LISTS_H
#include <exec/lists.h>
#endif
#ifndef  UTILITY_TAGITEM_H
#include <utility/tagitem.h>
#endif
#ifndef  MMU_CONTEXT_H
#include <mmu/context.h>
#endif

#ifndef MMU_BASE_NAME
#define MMU_BASE_NAME MMUBase
#endif

#define AllocAligned(bytesize, reqments, alignment) ({ \
  ULONG _AllocAligned_bytesize = (bytesize); \
  ULONG _AllocAligned_reqments = (reqments); \
  ULONG _AllocAligned_alignment = (alignment); \
  ({ \
  register char * _AllocAligned__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void * (*)(char * __asm("a6"), ULONG __asm("d0"), ULONG __asm("d1"), ULONG __asm("a0"))) \
  (_AllocAligned__bn - 30))(_AllocAligned__bn, _AllocAligned_bytesize, _AllocAligned_reqments, _AllocAligned_alignment); \
});})

#define GetMapping(ctx) ({ \
  struct MMUContext * _GetMapping_ctx = (ctx); \
  ({ \
  register char * _GetMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MinList * (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_GetMapping__bn - 36))(_GetMapping__bn, _GetMapping_ctx); \
});})

#define ReleaseMapping(ctx, list) ({ \
  struct MMUContext * _ReleaseMapping_ctx = (ctx); \
  struct MinList * _ReleaseMapping_list = (list); \
  ({ \
  register char * _ReleaseMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MinList * __asm("a1"))) \
  (_ReleaseMapping__bn - 42))(_ReleaseMapping__bn, _ReleaseMapping_ctx, _ReleaseMapping_list); \
});})

#define GetPageSize(ctx) ({ \
  struct MMUContext * _GetPageSize_ctx = (ctx); \
  ({ \
  register char * _GetPageSize__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_GetPageSize__bn - 48))(_GetPageSize__bn, _GetPageSize_ctx); \
});})

#define GetMMUType() ({ \
  register char * _GetMMUType__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((char (*)(char * __asm("a6"))) \
  (_GetMMUType__bn - 54))(_GetMMUType__bn); \
})

#define LockMMUContext(ctx) ({ \
  struct MMUContext * _LockMMUContext_ctx = (ctx); \
  ({ \
  register char * _LockMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_LockMMUContext__bn - 72))(_LockMMUContext__bn, _LockMMUContext_ctx); \
});})

#define UnlockMMUContext(ctx) ({ \
  struct MMUContext * _UnlockMMUContext_ctx = (ctx); \
  ({ \
  register char * _UnlockMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_UnlockMMUContext__bn - 78))(_UnlockMMUContext__bn, _UnlockMMUContext_ctx); \
});})

#define SetPropertiesA(ctx, flags, mask, lower, size, tags) ({ \
  struct MMUContext * _SetPropertiesA_ctx = (ctx); \
  ULONG _SetPropertiesA_flags = (flags); \
  ULONG _SetPropertiesA_mask = (mask); \
  ULONG _SetPropertiesA_lower = (lower); \
  ULONG _SetPropertiesA_size = (size); \
  struct TagItem * _SetPropertiesA_tags = (tags); \
  ({ \
  register char * _SetPropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("d1"), ULONG __asm("d2"), ULONG __asm("a1"), ULONG __asm("d0"), struct TagItem * __asm("a2"))) \
  (_SetPropertiesA__bn - 84))(_SetPropertiesA__bn, _SetPropertiesA_ctx, _SetPropertiesA_flags, _SetPropertiesA_mask, _SetPropertiesA_lower, _SetPropertiesA_size, _SetPropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ BOOL ___SetProperties(struct Library * MMUBase, struct MMUContext * ctx, ULONG flags, ULONG mask, ULONG lower, ULONG size, unsigned long tags, ...)
{
  return SetPropertiesA(ctx, flags, mask, lower, size, (struct TagItem *) &tags);
}

#define SetProperties(ctx, flags, mask, lower, size...) ___SetProperties(MMU_BASE_NAME, ctx, flags, mask, lower, size)
#endif

#define GetPropertiesA(ctx, lower, tags) ({ \
  struct MMUContext * _GetPropertiesA_ctx = (ctx); \
  ULONG _GetPropertiesA_lower = (lower); \
  struct TagItem * _GetPropertiesA_tags = (tags); \
  ({ \
  register char * _GetPropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("a1"), struct TagItem * __asm("a2"))) \
  (_GetPropertiesA__bn - 90))(_GetPropertiesA__bn, _GetPropertiesA_ctx, _GetPropertiesA_lower, _GetPropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ ULONG ___GetProperties(struct Library * MMUBase, struct MMUContext * ctx, ULONG lower, unsigned long tags, ...)
{
  return GetPropertiesA(ctx, lower, (struct TagItem *) &tags);
}

#define GetProperties(ctx, lower...) ___GetProperties(MMU_BASE_NAME, ctx, lower)
#endif

#define RebuildTree(ctx) ({ \
  struct MMUContext * _RebuildTree_ctx = (ctx); \
  ({ \
  register char * _RebuildTree__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_RebuildTree__bn - 96))(_RebuildTree__bn, _RebuildTree_ctx); \
});})

#define SetPagePropertiesA(ctx, flags, mask, lower, tags) ({ \
  struct MMUContext * _SetPagePropertiesA_ctx = (ctx); \
  ULONG _SetPagePropertiesA_flags = (flags); \
  ULONG _SetPagePropertiesA_mask = (mask); \
  ULONG _SetPagePropertiesA_lower = (lower); \
  struct TagItem * _SetPagePropertiesA_tags = (tags); \
  ({ \
  register char * _SetPagePropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("d1"), ULONG __asm("d2"), ULONG __asm("a1"), struct TagItem * __asm("a2"))) \
  (_SetPagePropertiesA__bn - 102))(_SetPagePropertiesA__bn, _SetPagePropertiesA_ctx, _SetPagePropertiesA_flags, _SetPagePropertiesA_mask, _SetPagePropertiesA_lower, _SetPagePropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ BOOL ___SetPageProperties(struct Library * MMUBase, struct MMUContext * ctx, ULONG flags, ULONG mask, ULONG lower, unsigned long tags, ...)
{
  return SetPagePropertiesA(ctx, flags, mask, lower, (struct TagItem *) &tags);
}

#define SetPageProperties(ctx, flags, mask, lower...) ___SetPageProperties(MMU_BASE_NAME, ctx, flags, mask, lower)
#endif

#define GetPagePropertiesA(ctx, lower, tags) ({ \
  struct MMUContext * _GetPagePropertiesA_ctx = (ctx); \
  ULONG _GetPagePropertiesA_lower = (lower); \
  struct TagItem * _GetPagePropertiesA_tags = (tags); \
  ({ \
  register char * _GetPagePropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("a1"), struct TagItem * __asm("a2"))) \
  (_GetPagePropertiesA__bn - 108))(_GetPagePropertiesA__bn, _GetPagePropertiesA_ctx, _GetPagePropertiesA_lower, _GetPagePropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ ULONG ___GetPageProperties(struct Library * MMUBase, struct MMUContext * ctx, ULONG lower, unsigned long tags, ...)
{
  return GetPagePropertiesA(ctx, lower, (struct TagItem *) &tags);
}

#define GetPageProperties(ctx, lower...) ___GetPageProperties(MMU_BASE_NAME, ctx, lower)
#endif

#define CreateMMUContextA(tags) ({ \
  struct TagItem * _CreateMMUContextA_tags = (tags); \
  ({ \
  register char * _CreateMMUContextA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"), struct TagItem * __asm("a0"))) \
  (_CreateMMUContextA__bn - 114))(_CreateMMUContextA__bn, _CreateMMUContextA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ struct MMUContext * ___CreateMMUContext(struct Library * MMUBase, unsigned long tags, ...)
{
  return CreateMMUContextA((struct TagItem *) &tags);
}

#define CreateMMUContext(tags...) ___CreateMMUContext(MMU_BASE_NAME, tags)
#endif

#define DeleteMMUContext(ctx) ({ \
  struct MMUContext * _DeleteMMUContext_ctx = (ctx); \
  ({ \
  register char * _DeleteMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_DeleteMMUContext__bn - 120))(_DeleteMMUContext__bn, _DeleteMMUContext_ctx); \
});})

#define AllocLineVec(bytesize, reqments) ({ \
  ULONG _AllocLineVec_bytesize = (bytesize); \
  ULONG _AllocLineVec_reqments = (reqments); \
  ({ \
  register char * _AllocLineVec__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void * (*)(char * __asm("a6"), ULONG __asm("d0"), ULONG __asm("d1"))) \
  (_AllocLineVec__bn - 132))(_AllocLineVec__bn, _AllocLineVec_bytesize, _AllocLineVec_reqments); \
});})

#define PhysicalPageLocation(ctx, addr) ({ \
  struct MMUContext * _PhysicalPageLocation_ctx = (ctx); \
  ULONG _PhysicalPageLocation_addr = (addr); \
  ({ \
  register char * _PhysicalPageLocation__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("a1"))) \
  (_PhysicalPageLocation__bn - 138))(_PhysicalPageLocation__bn, _PhysicalPageLocation_ctx, _PhysicalPageLocation_addr); \
});})

#define SuperContext(ctx) ({ \
  struct MMUContext * _SuperContext_ctx = (ctx); \
  ({ \
  register char * _SuperContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_SuperContext__bn - 144))(_SuperContext__bn, _SuperContext_ctx); \
});})

#define DefaultContext() ({ \
  register char * _DefaultContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"))) \
  (_DefaultContext__bn - 150))(_DefaultContext__bn); \
})

#define EnterMMUContext(ctx, task) ({ \
  struct MMUContext * _EnterMMUContext_ctx = (ctx); \
  struct Task * _EnterMMUContext_task = (task); \
  ({ \
  register char * _EnterMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct Task * __asm("a1"))) \
  (_EnterMMUContext__bn - 156))(_EnterMMUContext__bn, _EnterMMUContext_ctx, _EnterMMUContext_task); \
});})

#define LeaveMMUContext(task) ({ \
  struct Task * _LeaveMMUContext_task = (task); \
  ({ \
  register char * _LeaveMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"), struct Task * __asm("a1"))) \
  (_LeaveMMUContext__bn - 162))(_LeaveMMUContext__bn, _LeaveMMUContext_task); \
});})

#define AddContextHookA(tags) ({ \
  struct TagItem * _AddContextHookA_tags = (tags); \
  ({ \
  register char * _AddContextHookA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct ExceptionHook * (*)(char * __asm("a6"), struct TagItem * __asm("a0"))) \
  (_AddContextHookA__bn - 168))(_AddContextHookA__bn, _AddContextHookA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ struct ExceptionHook * ___AddContextHook(struct Library * MMUBase, unsigned long tags, ...)
{
  return AddContextHookA((struct TagItem *) &tags);
}

#define AddContextHook(tags...) ___AddContextHook(MMU_BASE_NAME, tags)
#endif

#define RemContextHook(hook) ({ \
  struct ExceptionHook * _RemContextHook_hook = (hook); \
  ({ \
  register char * _RemContextHook__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct ExceptionHook * __asm("a1"))) \
  (_RemContextHook__bn - 174))(_RemContextHook__bn, _RemContextHook_hook); \
});})

#define AddMessageHookA(tags) ({ \
  struct TagItem * _AddMessageHookA_tags = (tags); \
  ({ \
  register char * _AddMessageHookA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct ExceptionHook * (*)(char * __asm("a6"), struct TagItem * __asm("a0"))) \
  (_AddMessageHookA__bn - 180))(_AddMessageHookA__bn, _AddMessageHookA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ struct ExceptionHook * ___AddMessageHook(struct Library * MMUBase, unsigned long tags, ...)
{
  return AddMessageHookA((struct TagItem *) &tags);
}

#define AddMessageHook(tags...) ___AddMessageHook(MMU_BASE_NAME, tags)
#endif

#define RemMessageHook(hook) ({ \
  struct ExceptionHook * _RemMessageHook_hook = (hook); \
  ({ \
  register char * _RemMessageHook__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct ExceptionHook * __asm("a1"))) \
  (_RemMessageHook__bn - 186))(_RemMessageHook__bn, _RemMessageHook_hook); \
});})

#define ActivateException(hook) ({ \
  struct ExceptionHook * _ActivateException_hook = (hook); \
  ({ \
  register char * _ActivateException__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct ExceptionHook * __asm("a1"))) \
  (_ActivateException__bn - 192))(_ActivateException__bn, _ActivateException_hook); \
});})

#define DeactivateException(hook) ({ \
  struct ExceptionHook * _DeactivateException_hook = (hook); \
  ({ \
  register char * _DeactivateException__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct ExceptionHook * __asm("a1"))) \
  (_DeactivateException__bn - 198))(_DeactivateException__bn, _DeactivateException_hook); \
});})

#define AttemptLockMMUContext(ctx) ({ \
  struct MMUContext * _AttemptLockMMUContext_ctx = (ctx); \
  ({ \
  register char * _AttemptLockMMUContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((LONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_AttemptLockMMUContext__bn - 204))(_AttemptLockMMUContext__bn, _AttemptLockMMUContext_ctx); \
});})

#define LockContextList() ({ \
  register char * _LockContextList__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"))) \
  (_LockContextList__bn - 210))(_LockContextList__bn); \
})

#define UnlockContextList() ({ \
  register char * _UnlockContextList__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"))) \
  (_UnlockContextList__bn - 216))(_UnlockContextList__bn); \
})

#define AttemptLockContextList() ({ \
  register char * _AttemptLockContextList__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((LONG (*)(char * __asm("a6"))) \
  (_AttemptLockContextList__bn - 222))(_AttemptLockContextList__bn); \
})

#define SetPropertyList(ctx, list) ({ \
  struct MMUContext * _SetPropertyList_ctx = (ctx); \
  struct MinList * _SetPropertyList_list = (list); \
  ({ \
  register char * _SetPropertyList__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MinList * __asm("a1"))) \
  (_SetPropertyList__bn - 228))(_SetPropertyList__bn, _SetPropertyList_ctx, _SetPropertyList_list); \
});})

#define TouchPropertyList(list) ({ \
  struct MinList * _TouchPropertyList_list = (list); \
  ({ \
  register char * _TouchPropertyList__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MinList * __asm("a1"))) \
  (_TouchPropertyList__bn - 234))(_TouchPropertyList__bn, _TouchPropertyList_list); \
});})

#define CurrentContext(task) ({ \
  struct Task * _CurrentContext_task = (task); \
  ({ \
  register char * _CurrentContext__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MMUContext * (*)(char * __asm("a6"), struct Task * __asm("a1"))) \
  (_CurrentContext__bn - 240))(_CurrentContext__bn, _CurrentContext_task); \
});})

#define DMAInitiate(ctx, addrptrptr, lengthptr, write) ({ \
  struct MMUContext * _DMAInitiate_ctx = (ctx); \
  void ** _DMAInitiate_addrptrptr = (addrptrptr); \
  ULONG * _DMAInitiate_lengthptr = (lengthptr); \
  BOOL _DMAInitiate_write = (write); \
  ({ \
  register char * _DMAInitiate__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("d1"), void ** __asm("a0"), ULONG * __asm("a1"), BOOL __asm("d0"))) \
  (_DMAInitiate__bn - 246))(_DMAInitiate__bn, _DMAInitiate_ctx, _DMAInitiate_addrptrptr, _DMAInitiate_lengthptr, _DMAInitiate_write); \
});})

#define DMATerminate(ctx) ({ \
  struct MMUContext * _DMATerminate_ctx = (ctx); \
  ({ \
  register char * _DMATerminate__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("d1"))) \
  (_DMATerminate__bn - 252))(_DMATerminate__bn, _DMATerminate_ctx); \
});})

#define PhysicalLocation(ctx, addrptrptr, lengthptr) ({ \
  struct MMUContext * _PhysicalLocation_ctx = (ctx); \
  void ** _PhysicalLocation_addrptrptr = (addrptrptr); \
  ULONG * _PhysicalLocation_lengthptr = (lengthptr); \
  ({ \
  register char * _PhysicalLocation__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("d1"), void ** __asm("a0"), ULONG * __asm("a1"))) \
  (_PhysicalLocation__bn - 258))(_PhysicalLocation__bn, _PhysicalLocation_ctx, _PhysicalLocation_addrptrptr, _PhysicalLocation_lengthptr); \
});})

#define RemapSize(ctx) ({ \
  struct MMUContext * _RemapSize_ctx = (ctx); \
  ({ \
  register char * _RemapSize__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"))) \
  (_RemapSize__bn - 264))(_RemapSize__bn, _RemapSize_ctx); \
});})

#define WithoutMMU(func) ({ \
  unsigned long (*_WithoutMMU_func)() = (func); \
  ({ \
  register char * _WithoutMMU__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), unsigned long (*)() __asm("a5"))) \
  (_WithoutMMU__bn - 270))(_WithoutMMU__bn, _WithoutMMU_func); \
});})

#define SetBusError(func, oldfuncptr) ({ \
  void (*_SetBusError_func)() = (func); \
  void (**_SetBusError_oldfuncptr)() = (oldfuncptr); \
  ({ \
  register char * _SetBusError__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), void (*)() __asm("a0"), void (**)() __asm("a1"))) \
  (_SetBusError__bn - 276))(_SetBusError__bn, _SetBusError_func, _SetBusError_oldfuncptr); \
});})

#define GetMMUContextData(ctx, tagid) ({ \
  struct MMUContext * _GetMMUContextData_ctx = (ctx); \
  ULONG _GetMMUContextData_tagid = (tagid); \
  ({ \
  register char * _GetMMUContextData__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("d0"))) \
  (_GetMMUContextData__bn - 282))(_GetMMUContextData__bn, _GetMMUContextData_ctx, _GetMMUContextData_tagid); \
});})

#define SetMMUContextDataA(ctx, tags) ({ \
  struct MMUContext * _SetMMUContextDataA_ctx = (ctx); \
  struct TagItem * _SetMMUContextDataA_tags = (tags); \
  ({ \
  register char * _SetMMUContextDataA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct TagItem * __asm("a1"))) \
  (_SetMMUContextDataA__bn - 288))(_SetMMUContextDataA__bn, _SetMMUContextDataA_ctx, _SetMMUContextDataA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ void ___SetMMUContextData(struct Library * MMUBase, struct MMUContext * ctx, unsigned long tags, ...)
{
  SetMMUContextDataA(ctx, (struct TagItem *) &tags);
}

#define SetMMUContextData(ctx...) ___SetMMUContextData(MMU_BASE_NAME, ctx)
#endif

#define NewMapping() ({ \
  register char * _NewMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MinList * (*)(char * __asm("a6"))) \
  (_NewMapping__bn - 294))(_NewMapping__bn); \
})

#define CopyMapping(from, to, base, length, mask) ({ \
  struct MinList * _CopyMapping_from = (from); \
  struct MinList * _CopyMapping_to = (to); \
  ULONG _CopyMapping_base = (base); \
  ULONG _CopyMapping_length = (length); \
  ULONG _CopyMapping_mask = (mask); \
  ({ \
  register char * _CopyMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MinList * __asm("a0"), struct MinList * __asm("a1"), ULONG __asm("d0"), ULONG __asm("d1"), ULONG __asm("d2"))) \
  (_CopyMapping__bn - 300))(_CopyMapping__bn, _CopyMapping_from, _CopyMapping_to, _CopyMapping_base, _CopyMapping_length, _CopyMapping_mask); \
});})

#define DupMapping(list) ({ \
  struct MinList * _DupMapping_list = (list); \
  ({ \
  register char * _DupMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((struct MinList * (*)(char * __asm("a6"), struct MinList * __asm("a0"))) \
  (_DupMapping__bn - 306))(_DupMapping__bn, _DupMapping_list); \
});})

#define CopyContextRegion(ctx, list, base, length, mask) ({ \
  struct MMUContext * _CopyContextRegion_ctx = (ctx); \
  struct MinList * _CopyContextRegion_list = (list); \
  ULONG _CopyContextRegion_base = (base); \
  ULONG _CopyContextRegion_length = (length); \
  ULONG _CopyContextRegion_mask = (mask); \
  ({ \
  register char * _CopyContextRegion__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MinList * __asm("a1"), ULONG __asm("d0"), ULONG __asm("d1"), ULONG __asm("d2"))) \
  (_CopyContextRegion__bn - 312))(_CopyContextRegion__bn, _CopyContextRegion_ctx, _CopyContextRegion_list, _CopyContextRegion_base, _CopyContextRegion_length, _CopyContextRegion_mask); \
});})

#define SetPropertiesMapping(ctx, list, base, length, mask) ({ \
  struct MMUContext * _SetPropertiesMapping_ctx = (ctx); \
  struct MinList * _SetPropertiesMapping_list = (list); \
  ULONG _SetPropertiesMapping_base = (base); \
  ULONG _SetPropertiesMapping_length = (length); \
  ULONG _SetPropertiesMapping_mask = (mask); \
  ({ \
  register char * _SetPropertiesMapping__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MinList * __asm("a1"), ULONG __asm("d0"), ULONG __asm("d1"), ULONG __asm("d2"))) \
  (_SetPropertiesMapping__bn - 318))(_SetPropertiesMapping__bn, _SetPropertiesMapping_ctx, _SetPropertiesMapping_list, _SetPropertiesMapping_base, _SetPropertiesMapping_length, _SetPropertiesMapping_mask); \
});})

#define SetMappingPropertiesA(list, flags, mask, lower, size, tags) ({ \
  struct MinList * _SetMappingPropertiesA_list = (list); \
  ULONG _SetMappingPropertiesA_flags = (flags); \
  ULONG _SetMappingPropertiesA_mask = (mask); \
  ULONG _SetMappingPropertiesA_lower = (lower); \
  ULONG _SetMappingPropertiesA_size = (size); \
  struct TagItem * _SetMappingPropertiesA_tags = (tags); \
  ({ \
  register char * _SetMappingPropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((int (*)(char * __asm("a6"), struct MinList * __asm("a0"), ULONG __asm("d1"), ULONG __asm("d2"), ULONG __asm("a1"), ULONG __asm("d0"), struct TagItem * __asm("a2"))) \
  (_SetMappingPropertiesA__bn - 324))(_SetMappingPropertiesA__bn, _SetMappingPropertiesA_list, _SetMappingPropertiesA_flags, _SetMappingPropertiesA_mask, _SetMappingPropertiesA_lower, _SetMappingPropertiesA_size, _SetMappingPropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ int ___SetMappingProperties(struct Library * MMUBase, struct MinList * list, ULONG flags, ULONG mask, ULONG lower, ULONG size, unsigned long tags, ...)
{
  return SetMappingPropertiesA(list, flags, mask, lower, size, (struct TagItem *) &tags);
}

#define SetMappingProperties(list, flags, mask, lower, size...) ___SetMappingProperties(MMU_BASE_NAME, list, flags, mask, lower, size)
#endif

#define GetMappingPropertiesA(list, lower, tags) ({ \
  struct MinList * _GetMappingPropertiesA_list = (list); \
  ULONG _GetMappingPropertiesA_lower = (lower); \
  struct TagItem * _GetMappingPropertiesA_tags = (tags); \
  ({ \
  register char * _GetMappingPropertiesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MinList * __asm("a0"), ULONG __asm("a1"), struct TagItem * __asm("a2"))) \
  (_GetMappingPropertiesA__bn - 330))(_GetMappingPropertiesA__bn, _GetMappingPropertiesA_list, _GetMappingPropertiesA_lower, _GetMappingPropertiesA_tags); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ ULONG ___GetMappingProperties(struct Library * MMUBase, struct MinList * list, ULONG lower, unsigned long tags, ...)
{
  return GetMappingPropertiesA(list, lower, (struct TagItem *) &tags);
}

#define GetMappingProperties(list, lower...) ___GetMappingProperties(MMU_BASE_NAME, list, lower)
#endif

#define BuildIndirect(ctx, address, props) ({ \
  struct MMUContext * _BuildIndirect_ctx = (ctx); \
  ULONG _BuildIndirect_address = (address); \
  ULONG _BuildIndirect_props = (props); \
  ({ \
  register char * _BuildIndirect__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("d0"), ULONG __asm("d1"))) \
  (_BuildIndirect__bn - 336))(_BuildIndirect__bn, _BuildIndirect_ctx, _BuildIndirect_address, _BuildIndirect_props); \
});})

#define SetIndirect(destination, logical, descriptor) ({ \
  ULONG * _SetIndirect_destination = (destination); \
  ULONG _SetIndirect_logical = (logical); \
  ULONG _SetIndirect_descriptor = (descriptor); \
  ({ \
  register char * _SetIndirect__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), ULONG * __asm("a0"), ULONG __asm("a1"), ULONG __asm("d0"))) \
  (_SetIndirect__bn - 342))(_SetIndirect__bn, _SetIndirect_destination, _SetIndirect_logical, _SetIndirect_descriptor); \
});})

#define GetIndirect(ctx, adt, address) ({ \
  struct MMUContext * _GetIndirect_ctx = (ctx); \
  struct AbstractDescriptor * _GetIndirect_adt = (adt); \
  ULONG * _GetIndirect_address = (address); \
  ({ \
  register char * _GetIndirect__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct AbstractDescriptor * __asm("a1"), ULONG * __asm("d0"))) \
  (_GetIndirect__bn - 348))(_GetIndirect__bn, _GetIndirect_ctx, _GetIndirect_adt, _GetIndirect_address); \
});})

#define RebuildTreesA(ctxptr) ({ \
  struct MMUContext ** _RebuildTreesA_ctxptr = (ctxptr); \
  ({ \
  register char * _RebuildTreesA__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext ** __asm("a0"))) \
  (_RebuildTreesA__bn - 360))(_RebuildTreesA__bn, _RebuildTreesA_ctxptr); \
});})

#ifndef NO_INLINE_STDARG
static __inline__ BOOL ___RebuildTrees(struct Library * MMUBase, struct MMUContext * ctxptr, ...)
{
  return RebuildTreesA((struct MMUContext **) &ctxptr);
}

#define RebuildTrees(tags...) ___RebuildTrees(MMU_BASE_NAME, tags)
#endif

#define RunOldConfig(func) ({ \
  unsigned long (*_RunOldConfig_func)() = (func); \
  ({ \
  register char * _RunOldConfig__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), unsigned long (*)() __asm("a5"))) \
  (_RunOldConfig__bn - 366))(_RunOldConfig__bn, _RunOldConfig_func); \
});})

#define SetIndirectArray(destination, descriptor, number) ({ \
  ULONG * _SetIndirectArray_destination = (destination); \
  ULONG * _SetIndirectArray_descriptor = (descriptor); \
  ULONG _SetIndirectArray_number = (number); \
  ({ \
  register char * _SetIndirectArray__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), ULONG * __asm("a0"), ULONG * __asm("a1"), ULONG __asm("d0"))) \
  (_SetIndirectArray__bn - 372))(_SetIndirectArray__bn, _SetIndirectArray_destination, _SetIndirectArray_descriptor, _SetIndirectArray_number); \
});})

#define GetPageUsedModified(ctx, address) ({ \
  struct MMUContext * _GetPageUsedModified_ctx = (ctx); \
  ULONG _GetPageUsedModified_address = (address); \
  ({ \
  register char * _GetPageUsedModified__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), ULONG __asm("a1"))) \
  (_GetPageUsedModified__bn - 378))(_GetPageUsedModified__bn, _GetPageUsedModified_ctx, _GetPageUsedModified_address); \
});})

#define MapWindow(targetctx, srcctx, window) ({ \
  struct MMUContext * _MapWindow_targetctx = (targetctx); \
  struct MMUContext * _MapWindow_srcctx = (srcctx); \
  struct ContextWindow * _MapWindow_window = (window); \
  ({ \
  register char * _MapWindow__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MMUContext * __asm("a1"), struct ContextWindow * __asm("d1"))) \
  (_MapWindow__bn - 420))(_MapWindow__bn, _MapWindow_targetctx, _MapWindow_srcctx, _MapWindow_window); \
});})

#define ReleaseContextWindow(window) ({ \
  struct ContextWindow * _ReleaseContextWindow_window = (window); \
  ({ \
  register char * _ReleaseContextWindow__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((void (*)(char * __asm("a6"), struct ContextWindow * __asm("a1"))) \
  (_ReleaseContextWindow__bn - 432))(_ReleaseContextWindow__bn, _ReleaseContextWindow_window); \
});})

#define RefreshContextWindow(window) ({ \
  struct ContextWindow * _RefreshContextWindow_window = (window); \
  ({ \
  register char * _RefreshContextWindow__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct ContextWindow * __asm("a0"))) \
  (_RefreshContextWindow__bn - 438))(_RefreshContextWindow__bn, _RefreshContextWindow_window); \
});})

#define MapWindowCached(targetctx, srcctx, window) ({ \
  struct MMUContext * _MapWindowCached_targetctx = (targetctx); \
  struct MMUContext * _MapWindowCached_srcctx = (srcctx); \
  struct ContextWindow * _MapWindowCached_window = (window); \
  ({ \
  register char * _MapWindowCached__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct MMUContext * __asm("a0"), struct MMUContext * __asm("a1"), struct ContextWindow * __asm("d1"))) \
  (_MapWindowCached__bn - 444))(_MapWindowCached__bn, _MapWindowCached_targetctx, _MapWindowCached_srcctx, _MapWindowCached_window); \
});})

#define LayoutContextWindow(window) ({ \
  struct ContextWindow * _LayoutContextWindow_window = (window); \
  ({ \
  register char * _LayoutContextWindow__bn __asm("a6") = (char *) (MMU_BASE_NAME);\
  ((BOOL (*)(char * __asm("a6"), struct ContextWindow * __asm("a0"))) \
  (_LayoutContextWindow__bn - 450))(_LayoutContextWindow__bn, _LayoutContextWindow_window); \
});})

#endif /*  _INLINE_MMU_H  */
