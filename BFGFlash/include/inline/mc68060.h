#ifndef _INLINE_MC68060_H
#define _INLINE_MC68060_H

#ifndef CLIB_MC68060_PROTOS_H
#define CLIB_MC68060_PROTOS_H
#endif

#ifndef  EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef  LIBRARIES_68060_H
#include <libraries/68060.h>
#endif

#ifndef MC68060_BASE_NAME
#define MC68060_BASE_NAME MC68060Base
#endif

#define FPUControl(flags, mask) ({ \
  ULONG _FPUControl_flags = (flags); \
  ULONG _FPUControl_mask = (mask); \
  ({ \
  register char * _FPUControl__bn __asm("a6") = (char *) (MC68060_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), ULONG __asm("d0"), ULONG __asm("d1"))) \
  (_FPUControl__bn - 222))(_FPUControl__bn, _FPUControl_flags, _FPUControl_mask); \
});})

#endif /*  _INLINE_MC68060_H  */
