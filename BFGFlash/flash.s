;APS00001223000000000000000000000000000000000000000000000000000000000000000000000000
;------------------------------------------------------------------------------
; Flash write/erase functions for the BFG9060 card by Matthias Heinrichs
;
; (C) 2022 Henryk Richter
;
; 1.1 (16-Aug-22): added 39SF0x0 to supported flash types
; 1.0 (28-Jul-22): initial working version for 29F0x0
;------------------------------------------------------------------------------
;
;29F010 and 29F040 are Flashes with 8 Sectors and the same programming/erase 
;procedure. As of this writing, both variants are supported here. In addition,
;the SST39SF0x0 series is supported since version 1.1.
;
;More flash types might follow at a later point in time
;
;
;-------------------- CAUTION! ------------------------------------------------
;Some things are missing here: In case that the flash was in use and kickstart
;modules are active, then erasing the flash will cause these modules to 
;disappear. Hence, the system will crash. The only robust solution is to 
;disable all interrupts, erase/write the flash and then reboot the system.
;The DISABLE/ENABLE pair can be disabled for debugging purposes. See the
;STOPINTS directive
;
;------------------------------------------------------------------------------
;/* detect flash manufacturer, size and organization, checks for hardware write protection */
;ASM long flashrw_init( ASMR(a0) struct flashrw *frw ASMREG(a0) );
;/* erase the flash or parts of it
;   note(1): will return an error if the start address is not on a sector boundary
;   note(2): will erase the whole flash if size==0
;   note(3): will round up "nbytes" to sector size
;
;   erasure is the prerequisite for writing
;*/
;long flashrw_erase( ASMR(a0) struct flashrw *frw ASMREG(a0), ASMR(d0) long start_offset ASMREG(d0), ASMR(d1) long nbytes ASMREG(d1) );

;/*
;  write data to flash from "start_offset", total number of bytes "nbytes"
;
;  may fail for old flashes, so check error codes
;*/
;long flashrw_write( struct flashrw *frw, long start_offset, long nbytes, unsigned char *data );

STOPINTS	EQU	0	; stop interrupts while accessing flash (1=yes)/(0=no)

	ifne	STOPINTS
	incdir	include:
	include	"exec/ables.i"
;	include	"hardware/custom.i"

	ifnd	_intena
_intena	EQU	$dff09a	
	endc

	else
DISABLE	MACRO
	NOP
	ENDM
ENABLE	MACRO
	NOP
	ENDM
	endc


	incdir	include:
	include	exec/types.i

;
;exported symbols
;
	XDEF	_flashrw_init
	XDEF	_flashrw_erase
	XDEF	_flashrw_write

;
;structures and constants
;
	STRUCTURE	flashrw,0
	LONG		frw_Base	 ;base address like 0xF00000
	LONG		frw_ADDRA	 ;chip address A for autoselect, typ. 0x5555
	LONG		frw_ADDRB	 ;chip address B for autoselect, typ. 0x2AAA
	LONG		frw_SectorSize	 ;0x10000 for F040, 0x4000 for F010
	LONG            frw_TotalBytes   ;0x80000 for F040, 0x20000 for F010
	BYTE		frw_Manufacturer ;Manufacturer code (0x1=AMD,0x20=ST,0xC2=Macronix,...)
	BYTE		frw_Model	 ;0x20=F010,0xA4=F040
	BYTE		frw_Reserved1	 ;
	BYTE		frw_Reserved2	 ;
	LABEL		flashrw_sizeof

FLASH_DEFAULT	EQU	$F00000
FLASH_DEFAULTA	EQU	$5555		;some datasheets say 5555, some 555 -> AM29F040 supports both
FLASH_DEFAULTB	EQU	$2AAA
ID_F010		EQU	$20
ID_F040		EQU	$A4

; SST39SF0x0
ID_MANU_SST	EQU	$BF
ID_SF010	EQU	$B5
ID_SF020	EQU	$B6
ID_SF040	EQU	$B7


FRWERR_OK		EQU	0
FRWERR_PARAMETER	EQU	-1	;wrong parameter
FRWERR_UNKNOWNFLASH	EQU	-2	;neither F010,nor F040
FRWERR_SECTORPROTECT	EQU	-3	;there are protected sectors in flash, cannot proceed
FRWERR_OVERFLOW		EQU	-4	;erase request too large
FRWERR_WRITEFAIL	EQU	-5	;failure writing

;
; Test code for assembly level debugger
;
	ifne	1
test:
	lea	test_struct(pc),a0
	bsr	_flashrw_init

	ifne	1
	lea	test_struct(pc),a0
	move.l	#0*65536,d0
	move.l	#8*65536,d1
	bsr	_flashrw_erase
	endc

	lea	test_struct(pc),a0
	lea	test_data(pc),a1
	move.l	#7*65536,d0
	moveq	#14,d1
	bsr	_flashrw_write

	lea	FLASH_DEFAULT,a1
	rts

test_struct:	ds.b	flashrw_sizeof
test_data:	dc.b	"This is a test",0
	even

	endc
;
;
; Init Flash
; - call this before the other two functions, it`ll detect
;   the flash, verify that the flash is writable and set up 
;   the "struct flashrw" contents 
;
;
;in:  A0 = struct flashrw *
;out: D0 = result
;
_flashrw_init:
	movem.l	d2-d4/a2/a6,-(sp)
	move.l	4.w,a6
	DISABLE

	move.l	a0,d0			;d0==0->wrong parameter
	beq	finit_err

	lea	FLASH_DEFAULT,A1
	move.l	a1,frw_Base(a0)


	;try defaults for ADDRA,ADDRB
	move.l	#FLASH_DEFAULTA,d2
	move.l	#FLASH_DEFAULTB,d3
	move.l	d2,frw_ADDRA(a0)
	move.l	d3,frw_ADDRB(a0)

	; obtain model and manufacturer
	move.b	#$AA,(a1,d2.l)
	move.b	#$55,(a1,d3.l)
	move.b	#$90,(a1,d2.l)
	move.b	(a1),d4
	move.b	d4,frw_Manufacturer(a0)
	move.b	1(a1),d4
	move.b	d4,frw_Model(a0)

	moveq	#FRWERR_UNKNOWNFLASH+1,d0

	cmp.b	#ID_F010,d4	;F010
	beq.s	.is_F010
	moveq	#FRWERR_UNKNOWNFLASH+1,d0
	cmp.b	#ID_F040,d4	;F040
	beq.s	.is_F040

	cmp.b	#ID_MANU_SST,frw_Manufacturer(a0)
	bne	finit_err_unlock

	moveq	#0,d0
	bset	#12,d0			;all SST39SFxxx have 4kB sectors
	move.l	d0,frw_SectorSize(a0)	;$1000
	lsl.l	#5,d0			;$20000
	cmp.b	#ID_SF010,d4
	beq.s	.sst_found
	add.l	d0,d0			;$40000
	cmp.b	#ID_SF020,d4
	beq.s	.sst_found
	add.l	d0,d0			;$80000
	cmp.b	#ID_SF040,d4
	bne	finit_err_unlock
.sst_found:
	move.l	d0,frw_TotalBytes(a0)	;$80000
	moveq	#0,d0			;no sector protection detected (TODO)
	bra.s	.sst_skip_sector_check
.is_F040:
	moveq	#0,d0
	bset	#16,d0	;
	move.l	d0,frw_SectorSize(a0)	;$10000
	lsl.l	#3,d0
	move.l	d0,frw_TotalBytes(a0)	;$80000

	bra.s	.have_flash
.is_F010:
	moveq	#0,d0
	bset	#14,d0	;
	move.l	d0,frw_SectorSize(a0)	;$4000
	lsl.l	#3,d0
	move.l	d0,frw_TotalBytes(a0)	;$20000
.have_flash:

	;now check sector protection
	move.l	a1,a2
	moveq	#8-1,d1
	moveq	#0,d0			;0 protected sectors
.sectorcheck:
	add.b	2(a2),d0
	add.l	frw_SectorSize(a0),a2
	dbf	d1,.sectorcheck
.sst_skip_sector_check:
	; reset: back to regular mode
	move.b	#$f0,(a1)


	;d4=model,d0=sector protection count
	cmp.b	#0,d0			;all sectors unprotected?
	beq.s	.proceed
	moveq	#FRWERR_SECTORPROTECT+1,d0
	bra.s	finit_err
.proceed:
	;ok, we have F010 or F040, all fields set up, return happily

	;TODO: maybe check manufacturer code
	;and return FRWERR_UNKNOWNFLASH if unknown
	;
	;Manufacturer ID: AMD 0x01 (also TI)
	;                 ST  0x20
	;	     Macronix 0xC2
	;           Microchip 0xBF


.ok:	moveq	#FRWERR_OK,d0
.rts:
finit_rts:
	move.l	4.w,a6
	ENABLE
	movem.l	(sp)+,d2-d4/a2/a6
	rts

finit_err_unlock:
	; reset: back to regular mode (might be irrelevant, we didn`t find a usable Flash)
	move.b	#$f0,(a1)
	
; error_code + 1 -> error_code
finit_err:
	subq.l	#1,d0
	bra.s	finit_rts

;
;/* erase the flash or parts of it
;   note(1): will return an error if the start address is not on a sector boundary
;   note(2): will erase the whole flash if size==0
;   note(3): will round up "nbytes" to sector size
;
;   erasure is the prerequisite for writing
; in:
;   A0 = struct flashrw *
;   D0 = long start_offset
;   D1 = long nbytes
;out:
;   D0 = error code
;*/
_flashrw_erase:
	movem.l	d2-d5/a2/a6,-(sp)
	move.l	4.w,a6
	DISABLE

	;----------------------------------------------------------------------
	;parameter check
	;

	move.l	d0,d4			;start offset
	move.l	a0,d0			;d0==0->wrong parameter
	beq	.err
	move.l	frw_Base(a0),d0		;struct initialized?
	beq	.err

	move.l	frw_SectorSize(a0),d0	;check start alignment
	subq.l	#1,d0			;
	and.l	d4,d0			;
	bne	ferase_err_align	;start is not on a sector boundary

	move.l	frw_SectorSize(a0),d0	;check start alignment
	subq.l	#1,d0
	add.l	d0,d1			;size + (sector_length-1)
	not.l	d0			;~(sector_size-1) = mask
	and.l	d0,d1			;rounded up size

	;now check start + erase length <= total size
	move.l	d1,d0			;size
	add.l	d4,d0			;+start offset
	cmp.l	frw_TotalBytes(a0),d0
	bgt	ferase_err_overflow	;too much erase requested

	;----------------------------------------------------------------------
	;command mode, erase cmd
	;
	move.l	frw_Base(a0),a1
	lea	(a1,d4.l),a2		;first sector
	move.l	frw_ADDRA(a0),d2
	move.l	frw_ADDRB(a0),d3
.erase_loop:
	move.b	#$AA,(a1,d2.l)
	move.b	#$55,(a1,d3.l)
	move.b	#$80,(a1,d2.l)
	move.b	#$AA,(a1,d2.l)
	move.b	#$55,(a1,d3.l)

	move.b	#$30,(a2)		;erase command for sector address 

	;----------------------------------------------------------------------
	;wait loop
	;d4 = dat
	;d0 = new dat
	move.b	(a2),d4			;first data Byte
.erase_waitloop:
	move.b	(a2),d0			;next data Byte
	move.b	d4,d5			;old byte
	eor.b	d0,d5			;^new byte
	btst	#6,d5			;did bit 6 toggle ?
	beq.s	.erase_done		;bit 6 is cleared, hence it didn`t toggle
	move.b	d0,d4
	btst	#5,d0			;if( !DAT & (1<<5) )
	beq.s	.erase_waitloop	
	
	move.b	(a2),d4			;first data Byte
	move.b	(a2),d0			;next data Byte
	move.b	d4,d5			;old byte
	eor.b	d0,d5			;^new byte
	btst	#6,d5			;did bit 6 toggle ?
	beq.s	.erase_done		;bit 6 is cleared, hence it didn`t toggle

	;error
	move.b	#$f0,(a1)
	moveq	#FRWERR_WRITEFAIL+1,d0
	bra.s	.err
.erase_done: ; 1 sector down
	
	;----------------------------------------------------------------------
	;next sector
	;
	add.l	frw_SectorSize(a0),a2	;next sector
	sub.l	frw_SectorSize(a0),d1	;remaining size
	bgt.s	.erase_loop	

	;----------------------------------------------------------------------
	; reset: back to regular mode
	;
	move.b	#$f0,(a1)
	;
	;DONE
	;
	moveq	#1,d0			;will result in OK after subq #1
.err:
ferase_err:
	subq.l	#1,d0
	
	ENABLE
	movem.l	(sp)+,d2-d5/a2/a6
	rts
ferase_err_align:
	moveq	#0,d0
	bra.s	ferase_err
ferase_err_overflow:
	moveq	#FRWERR_OVERFLOW,d0
	bra.s	ferase_err

; in:
;   A0 = struct flashrw *
;   D0 = long start_offset
;   D1 = long nbytes
;   A1 = unsigned char *data
;out:
;   D0 = error code
_flashrw_write:
	movem.l	d2-d4/a2-a3/a6,-(sp)
	move.l	4.w,a6
	DISABLE
	
	move.l	a0,d2			;d2==0->wrong parameter
	beq	fwrite_err_param
	move.l	frw_Base(a0),d2		;struct initialized?
	beq	fwrite_err_param

	move.l	d0,d2			;start offset
	add.l	d1,d2			;length in bytes
	cmp.l	frw_TotalBytes(a0),d0
	bgt.s	fwrite_err_overflow	;too much write requested

	;----------------------------------------------------------------------
	;enter main loop
	;
	move.l	frw_Base(a0),a3
	lea	(a3,d0.l),a2		;first byte to be written
	move.l	frw_ADDRA(a0),d2
	move.l	frw_ADDRB(a0),d3
	;;
.write_loop:
	move.b	#$AA,(a3,d2.l)		;magic unlock sequence
	move.b	#$55,(a3,d3.l)		;-> flash returns to normal mode 
	move.b	#$A0,(a3,d2.l)		;   after each successful write

	move.b	(a1)+,d0		;DAT
	move.b	d0,(a2)			;write DAT byte

	;----------------------------------------------------------------------
	;wait loop
.write_wait_loop:
	move.b	(a2),d4			;DATB
	cmp.b	d4,d0			;DAT==DATB -> good, next byte
	beq.s	.write_bytedone

	btst	#5,d4			;bit 5 == 0 ?
	beq.s	.write_wait_loop	;yep, wait some more

	cmp.b	(a2),d0			;DAT==DATB ?
	beq.s	.write_bytedone

	move.b	#$f0,(a3)		;failure, need to reset chip manually

	moveq	#FRWERR_WRITEFAIL+1,d0	;sorry, can`t write data
	bra.s	fwrite_err
.write_bytedone:

	addq.l	#1,a2			;next byte
	subq.l	#1,d1
	bgt.s	.write_loop

	;
	;DONE
	;

	moveq	#1,d0			;will result in OK after subq #1
.err:
fwrite_err:
	subq.l	#1,d0

	ENABLE
	movem.l	(sp)+,d2-d4/a2-a3/a6
	rts
fwrite_err_param:
	moveq	#0,d0
	bra.s	fwrite_err
fwrite_err_overflow:
	moveq	#FRWERR_OVERFLOW,d0
	bra.s	fwrite_err



	end

