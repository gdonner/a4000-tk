; simple glue to embed binary into object file
;
; (C) 2022 Henryk Richter
;
	xdef	_bootrom_bin
	xdef	_bootrom_size

	section	1,data

_bootrom_bin:
	incbin	"bootrom.bin"
_bootrom_size:
	dc.l	*-_bootrom_bin
