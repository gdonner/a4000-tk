/*
  lists.h

  double linked list and node handling function headers

*/
#ifndef _INCL_LISTS_H
#define _INCL_LISTS_H

/* node (leaving out ln_Type makes this actually a MinNode) */
struct node
{
    struct node * ln_Succ,
                * ln_Pred;
/*    Int32         ln_Type; */
};

/* list (Minimal double linked list) */
struct nodelist
{
    struct node * lh_Head;
    struct node * lh_Tail;
    struct node * lh_TailPred;
/*    Int32         lh_Type; */
};

/* macros */
#define ISLISTEMPTY(l) \
        ( (((struct nodelist *)l)->lh_TailPred) == (struct node *)(l) )

#define NEWLIST(l)       (((struct nodelist *)l)->lh_TailPred \
                                = (struct node *)(l), \
                            ((struct nodelist *)l)->lh_Tail = 0, \
                            ((struct nodelist *)l)->lh_Head \
                                = (struct node *)\
                                    &(((struct nodelist *)l)->lh_Tail))

#define ADDHEAD(l,n)     ((void)(\
        ((struct node *)n)->ln_Succ          = ((struct nodelist *)l)->lh_Head, \
        ((struct node *)n)->ln_Pred          = (struct node *)&((struct nodelist *)l)->lh_Head, \
        ((struct nodelist *)l)->lh_Head->ln_Pred = ((struct node *)n), \
        ((struct nodelist *)l)->lh_Head          = ((struct node *)n)))

#define ADDTAIL(l,n)     ((void)(\
        ((struct node *)n)->ln_Succ              = (struct node *)&((struct nodelist *)l)->lh_Tail, \
        ((struct node *)n)->ln_Pred              = ((struct nodelist *)l)->lh_TailPred, \
        ((struct nodelist *)l)->lh_TailPred->ln_Succ = ((struct node *)n), \
        ((struct nodelist *)l)->lh_TailPred          = ((struct node *)n) ))

#define REMOVE(n)        ((void)(\
        ((struct node *)n)->ln_Pred->ln_Succ = ((struct node *)n)->ln_Succ,\
        ((struct node *)n)->ln_Succ->ln_Pred = ((struct node *)n)->ln_Pred ))

#define GetHead(l)       (void *)(((struct nodelist *)l)->lh_Head->ln_Succ \
                                ? ((struct nodelist *)l)->lh_Head \
                                : (struct node *)0)
#define GetTail(l)       (void *)(((struct nodelist *)l)->lh_TailPred->ln_Pred \
                                ? ((struct nodelist *)l)->lh_TailPred \
                                : (struct node *)0)
#define GetSucc(n)       (void *)(((struct node *)n)->ln_Succ->ln_Succ \
                                ? ((struct node *)n)->ln_Succ \
                                : (struct node *)0)
#define GetPred(n)       (void *)(((struct node *)n)->ln_Pred->ln_Pred \
                                ? ((struct node *)n)->ln_Pred \
                                : (struct node *)0)


#endif /* _INCL_LISTS_H */
