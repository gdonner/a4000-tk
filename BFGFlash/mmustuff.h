/*
  set up MMU for flash access using mmu.library

  (C) 2022 Henryk Richter

  The F0 area (0xF00000-0xF7ffff) is known to be
  reserved by Commodore for diagnostic ROMs (or
  BootROMs), as well as additional kickstart modules.

  Some configurations of the MMU will mark this
  area as ROM such that write attempts are inhibited
  by the processor itself.

  This code will revert any existing remapping (to
  RAM) and set up useable cache flags such that the
  Flash can be written to.

*/
#ifndef _INC_MMUSTUFF_H
#define _INC_MMUSTUFF_H

#include <exec/types.h>

LONG setupMMU( void *base, LONG size, ULONG flags );

#endif
