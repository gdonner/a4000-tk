/*
;------------------------------------------------------------------------------
; Flash write/erase functions for the BFG9060 card by Matthias Heinrichs
;
; (C) 2022 Henryk Richter
;------------------------------------------------------------------------------
;
;29F010 and 29F040 are Flashes with 8 Sectors and the same programming/erase 
;procedure. As of this writing, both variants are supported here. Additional
;flash types might follow at a later point in time
;
;-------------------- CAUTION! ------------------------------------------------
;Some things are missing here: In case that the flash was in use and kickstart
;modules are active, then erasing the flash will cause these modules to 
;disappear. Hence, the system will crash. The only robust solution is to 
;disable all interrupts, erase/write the flash and then reboot the system.
;
;------------------------------------------------------------------------------
*/
#ifndef _INC_FLASH_H
#define _INC_FLASH_H

#include <exec/types.h>
#include "compiler.h"

/*
;
;structures and constants
;
*/
struct flashrw {
	ULONG		frw_Base;	  /* ;base address like 0xF00000 */
	ULONG		frw_ADDRA;	  /* ;chip address A for autoselect, typ. 0x5555 */
	ULONG		frw_ADDRB;	  /* ;chip address B for autoselect, typ. 0x2AAA */
	ULONG		frw_SectorSize;	  /* ;0x10000 for F040, 0x4000 for F010  */
	ULONG           frw_TotalBytes;   /* ;0x80000 for F040, 0x20000 for F010 */
	UBYTE		frw_Manufacturer; /* ;Manufacturer code (0x1=AMD,0x20=ST,0xC2=Macronix,...) */
	UBYTE		frw_Model;	  /* ;0x20=F010,0xA4=F040 */
	BYTE		frw_Reserved1;	  /* ; */
	BYTE		frw_Reserved2;	  /* ; */
};

/* these are to be found/decided/used in flash.s but are 
   NOT of concern for the external API, i.e. you need
   frw_TotalBytes for write/erase ops after flash_init(),
   perhaps frw_SectorSize but the rest should be opaque */
#define FLASH_DEFAULT	0xF00000
#define FLASH_TOTALSIZE 0x080000	/* don't use when talking to flash, this is the full area, regardless of flash type */
#define FLASH_DEFAULTA	0x5555		/* ;some datasheets say 5555, some 555 -> AM29F040 supports both */
#define FLASH_DEFAULTB	0x2AAA
#define ID_F010		0x20
#define ID_F040		0xA4

#define FRWERR_OK		0
#define FRWERR_PARAMETER	-1	/* ;wrong parameter */
#define FRWERR_UNKNOWNFLASH	-2	/* ;neither F010,nor F040 */
#define FRWERR_SECTORPROTECT	-3	/* ;there are protected sectors in flash, cannot proceed */
#define FRWERR_OVERFLOW		-4	/* ;erase request too large */
#define FRWERR_WRITEFAIL	-5	/* ;failure writing */
#define FRWERR_UNSPECIFIED	-6	/* ;some other error */

/* detect flash manufacturer, size and organization, checks for hardware write protection */
ASM long flashrw_init( ASMR(a0) struct flashrw *frw ASMREG(a0) );

/* erase the flash or parts of it
;   note(1): will return an error if the start address is not on a sector boundary
;   note(2): will erase the whole flash if size==0
;   note(3): will round up "nbytes" to sector size
;
;   erasure is the prerequisite for writing
*/
ASM long flashrw_erase( ASMR(a0) struct flashrw *frw ASMREG(a0), 
                        ASMR(d0) long start_offset ASMREG(d0), 
                        ASMR(d1) long nbytes ASMREG(d1) );

/*
;  write data to flash from "start_offset", total number of bytes "nbytes"
;
;  may fail for old flashes, so check error codes
*/
ASM long flashrw_write( ASMR(a0) struct flashrw *frw ASMREG(a0), 
                        ASMR(d0) long start_offset ASMREG(d0), 
                        ASMR(d1) long nbytes ASMREG(d1),
                        ASMR(a1) unsigned char *data ASMREG(a1) );

#endif /* _INC_FLASH_H */
