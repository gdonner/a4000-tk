/*
  set up MMU for flash access using mmu.library

  (C) 2022 Henryk Richter

  The F0 area (0xF00000-0xF7ffff) is known to be
  reserved by Commodore for diagnostic ROMs (or
  BootROMs), as well as additional kickstart modules.

  Some configurations of the MMU will mark this
  area as ROM such that write attempts are inhibited
  by the processor itself.

  This code will revert any existing remapping (to
  RAM) and set up useable cache flags such that the
  Flash can be written to.

*/
#include <proto/dos.h>
#include <proto/mmu.h>
#include <mmu/context.h>
#include <mmu/mmutags.h>

extern struct Library *MMUBase;

#define CACHEFLAGS (MAPP_CACHEINHIBIT|MAPP_COPYBACK|MAPP_NONSERIALIZED|MAPP_IMPRECISE)

#define ROMFLAGS  (MAPP_CACHEINHIBIT|MAPP_GLOBAL|MAPP_IO)
#define ROMCFLAGS (MAPP_CACHEINHIBIT|MAPP_COPYBACK|MAPP_NONSERIALIZED|MAPP_IMPRECISE|MAPP_GLOBAL|MAPP_IO|MAPP_WRITEPROTECTED|MAPP_ROM)

/*
  set up MMU as I/O space from "base", "size" bytes,
  keep flags==0 for now
*/
LONG setupMMU( void *base, LONG size, ULONG flags )
{
	struct MMUContext *ctx,*sctx;   /* default context, supervisor context */
	ULONG  props;
	struct MinList *ctxl,*sctxl;

	if( !MMUBase )
		return 1;	/* cannot setup MMU without MMULib */

	ctx=DefaultContext();
	sctx=SuperContext(ctx);

	props  = GetProperties(sctx,(ULONG)base,TAG_DONE);
	props |= GetProperties(sctx,(ULONG)base,TAG_DONE);

	Printf( (STRPTR)"Current MMU props on 0x%lx area: 0x%lx\n",(ULONG)base,props);

	LockContextList();
	LockMMUContext(ctx);
	LockMMUContext(sctx);

	if( (props & MAPP_REMAPPED) )
	{
		Printf( (STRPTR)"Memory was remapped elsewhere, reverting to physical location\n" );

	        if( (ctxl=GetMapping(ctx)))
		{
        	 if( (sctxl=GetMapping(sctx)))
		 {
		  if( SetProperties(ctx,0,MAPP_REMAPPED|CACHEFLAGS,(ULONG)base,size,TAG_DONE) )
		  {
		   if( SetProperties(sctx,0,MAPP_REMAPPED|CACHEFLAGS,(ULONG)base,size,TAG_DONE) )
		   {
		   }
		  }
		  ReleaseMapping(sctx,sctxl);
		 }
		 ReleaseMapping(ctx,ctxl);
		}
	}

	/* set desired flags */
	SetProperties(ctx,ROMFLAGS,ROMCFLAGS,(ULONG)base,size,TAG_DONE);
	SetProperties(sctx,ROMFLAGS,ROMCFLAGS,(ULONG)base,size,TAG_DONE);

	RebuildTrees(ctx,sctx,NULL); 

        UnlockMMUContext(sctx);
       	UnlockMMUContext(ctx);
       	UnlockContextList();

	props  = GetProperties(sctx,(ULONG)base,TAG_DONE);
	props |= GetProperties(sctx,(ULONG)base,TAG_DONE);
	Printf( (STRPTR)"New MMU props on 0x%lx area: 0x%lx\n",(ULONG)base,props);

	return 0;
}

