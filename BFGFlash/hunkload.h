/*
  (C) 2022 Henryk Richter
*/
#ifndef _INC_HUNKLOAD_H
#define _INC_HUNKLOAD_H
#include "compiler.h"
#include "lists.h"

/* this is the offset applied to code, data and bss sections when loading
   with respect to the pointer in struct hunk. The reason is that an
   offset of 4 allows the loaded file to be re-used as AmigaDOS segment 
   list
*/
#define LOAD_OFFSET 4

/*
  Enable the #define below for Loadseg compatible operation, which
  then loads the hunks into the correct memory type. If undefined, then
  any memory type is acceptable. Requires LOAD_OFFSET==4 for practical
  use.
*/
#undef  LOADSEG_COMPAT
#define LOADSEG_COMPAT


#define A_UNKNOWN  0
/*
  regular reloc entries, format:
   while( NReloc (ULONG) > 0 )  - 32 bit big endian count
   {
     HUNKIDX (ULONG)            - 32 bit big endian referenced hunk index
     NReloc*ULONG               - relocation offsets to start of current hunk
   }
*/
#define A_RELOC16  1
#define A_RELOC32  2
#define A_RELRELOC32 3
/* compressed reloc entries, format is TBD */
#define A_CRELOC16 4
#define A_CRELOC32 5
#define A_CRELRELOC32 6
/* */

struct attribs
{
	struct node n;
	unsigned long type; /* A_RELOC16,A_RELOC32,A_CRELOC16,A_CRELOC32,... */
	unsigned long size; /* number of relocation entries */
	unsigned long dest; /* destination hunk number to relocate for */
	/* the relocs themselves follow directly after here */
};

struct hunk
{
	struct node n;
	unsigned long type;	 /* code, data, bss,... */
	unsigned long memtype;   /* memory type (default,fast,chip) */
	unsigned long flags;     /* compressed/uncompressed */
	unsigned long size;      /* size of hunk contents, without sizeof(struct hunk) */
	unsigned char *p;        /* memory for hunk contents (possibly padded in front, see LOAD_OFFSET) */
	struct nodelist attribs; /* reloc and such     */
};

struct hunkload
{
	struct node n;           /* for your convenience: put multiple loadfiles into a list */
	unsigned long totalsize; /* calculated from header */
	unsigned long firsthunk; /* from file */
	unsigned long lasthunk;  /* from file */
	unsigned long nhunks;    /* from file */
	struct 	nodelist hunks;
};

/* load hunk executable file, parse and store relevant data*/
struct hunkload *hunk_load( char *path, unsigned long flags, long *errcode );

/* error codes */
#define HLERR_OK	0	/* all good */
#define HLERR_FILEERR	1	/* file not found or read error */
#define HLERR_MEMORY    2	/* out of memory */
#define HLERR_NOTEXE	3	/* not a hunk executable */
#define HLERR_UNKRELOC	4	/* unknown/unsupported relocation method */
#define HLERR_OVERLAY	5	/* overlays are not supported for this code (i.e. we want something ROM-able) */
#define HLERR_WRONGARG	6	/* general error: wrong argument */
#define HLERR_UNIMPLEMENTED 7	/* unimplemented feature */

/* compress code and data of hunk executable */
long hunk_pack( struct hunkload *hunkfile );

/* decompress code and data of hunk executable */
long hunk_unpack( struct hunkload *hunkfile );

/* relocate executable to specific location (consecutive code, data, bss) */
/* base_address - is the location where the code is eventually run from,
   save_address - specifies where to copy the resulting code/data/bss contents, i.e.
                  this way you can build a rom image at "save_address" 
   flags        - TBD 

   notes: the relocation is not performed on compressed hunk files, which means that the
          compressed file is just stored at the "save address" including the reloc information
*/
long hunk_relocate( struct hunkload *hunkfile, void *base_address, void *save_address, unsigned long flags );



/* release file */
long hunk_free( struct hunkload *hunkfile );

#endif /* _INC_HUNKLOAD_H */
